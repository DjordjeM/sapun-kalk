<?php

add_shortcode( 'sapunkalk', 'sapunkalk_render' );

function sapunkalk_render(){
?>

	<form id="recipeData" name="recipeData" action="" method="post" target="">
	
	
		<div class="container">
				<div class="row">
					<div class="col-md-12 column">
						<div class="row">
							<div class="col-md-2 column">
								<div class="row">
										<div class="col-md-12 column">Tip baze<br></div>
									</div>
								<div class="row">
									<input style="margin:5px 0px;" type="radio" id="hydroxide0" name="R2" value="0" checked="checked" onclick="javascript:hydroxideChange(this);"  onchange="javascript:textChanged();"/>
									<label id="lblNaOH" for="hydroxide0">&nbsp;<strong>NaOH</strong></label>
								</div>
								<div class="row">
									<input style="margin:5px 0px;" type="radio" id="hydroxide1" name="R2" value="1" onclick="javascript:hydroxideChange(this);" onchange="javascript:textChanged();" />
									<label id="lblKOH" for="hydroxide1">&nbsp;KOH</label>
								</div>
								<div class="row">
									<input id="chkNintyPerCent" name="chkNintyPerCent" type="checkbox" onchange="javascript:kohPurityChanged()" title="Check if your KOH is 90% pure" />
									<label for="chkNintyPerCent" title="Check if your KOH is 90% pure">&nbsp; 90% KOH</label>
								</div>
							</div>
							<div class="col-md-2 column">
								<div class="row">
										<div class="col-md-12 column">Tezina ulja<br></div>
									</div>
								<div class="row">
									<input type="radio" id="oilWeight2" name="R1" value="2" checked="checked" onclick="twOnClick()" onchange="javascript:textChanged();" /><label id="lbloilWeight2" for="oilWeight2">Gram</label>
								</div>
								<div class="row">
									<input type="radio" id="oilWeight0" name="R1" value="0" onclick="twOnClick()" onchange="javascript:textChanged();" /><label id="lbloilWeight0" for="oilWeight0" style="font-weight:normal">Funte</label>
								</div>
								<div class="row">
									<input type="radio" id="oilWeight1" name="R1" value="1" onclick="twOnClick()" onchange="javascript:textChanged();" /><label id="lbloilWeight1" for="oilWeight1" style="font-weight:normal">Unce</label>
								</div>
								<div class="row">
									<div class="col-md-6 column">
										<input class="inputL" type="text" id="txtTW" name="txtTW" size="9" value="1" style="background-color:#C9FCD0;" onchange="javascript:textChanged();" />
									</div>
									<div class="col-md-6 column" id="TWunit"> lb</div>
								</div>
							</div>
							<div class="col-md-4 column">
									<div class="row">
										<div class="col-md-12 column">Voda</div>
									</div>
									 <div class="row">
										<div class="col-md-6 column">
											<input type="radio" id="waterAmount0" name="R4" value="0" checked="checked" onclick="javascript:setFixedWaterMethod(this)" onchange="javascript:textChanged();" /><label id="lblwaterAmount0" for="waterAmount0">&nbsp; Water as &#37; of Oils</label>
										</div>
										<div class="col-md-6 column">
											<input name="txtWaterPercent" type="text" id="txtWaterPercent" value="38"  maxlength="7" size="3" style="background-color:#C9FCD0;" onchange="javascript:textChanged();" />
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 column">
											<input type="radio" id="waterAmount1" name="R4" value="1" onclick="javascript:setFixedWaterMethod(this)" onchange="javascript:textChanged();" /><label id="lblwaterAmount1" for="waterAmount1" style="font-weight:normal">&nbsp;  Lye Concentration</label>
										</div>
										<div class="col-md-6 column">
											<input name="spnLyeConc" type="text" size="3" class="inputRRO" id="spnLyeConc" readonly="readonly" value="" style="background-color:#FFFFFF;" onchange="javascript:textChanged();" />
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 column">
											<input type="radio" id="waterAmount2" name="R4" value="2" onclick="javascript:setFixedWaterMethod(this)" onchange="javascript:textChanged();" /><label id="lblwaterAmount2" for="waterAmount2" style="font-weight:normal">&nbsp;  Water : Lye Ratio</label>
										</div>
										<div class="col-md-6 column">
											<input id="spnWaterLyeRatio" name="spnWaterLyeRatio" type="text" size="3" class="inputRRO" readonly="readonly" style="background-color:#FFFFFF;" onchange="javascript:textChanged();" />
										</div>
									</div>
								
								</div>
								<div class="col-md-4 column">
									<div class="row">
										<div class="col-md-12 column">&nbsp;</div>
									</div>
									<div class="row">
										<div class="col-md-3 column">Popust</div>
										<div class="col-md-9 column">
											<input name="txtDiscount" type="text" id="txtDiscount"  value="5"  size="3" maxlength="8" style="background-color:#C9FCD0;width:35px;" onchange="javascript:textChanged();" /> &nbsp;%
										</div>
										<div class="col-md-3 column">Miris</div>
										<div class="col-md-9 column">
											<input class="inputL" type="text" id="txtFragranceRatio" name="txtFragranceRatio" value="0.5" size="3"style="width:35px;background-color:#C9FCD0;" onchange="javascript:textChanged();" />	
										</div>
										<div class="col-md-3 column">Kolicina</div>
										<div class="col-md-9 column">
											<div class="row">
											<div class="col-md-3 column">
												<input id="spnFragranceRequired" name="spnFragranceRequired" type="text" size="3" class="inputRRO"  readonly="readonly" />
											</div>
											<div class="col-sm-9 column" id="FragranceRatioUnit">Oz/Lb</div>
											</div>
										</div>
									</div>
								</div>
						</div>
							<div class="row">
								<div class="col-md-5 column">
									<div class="row">
										<div class="col-md-6 column">
										&nbsp 
											<div class="row">&nbsp</div>
											<div class="row">
												<div class="col-md-4 column" " title="29 to 54">Cvrstoca</div> <!-- onmouseover="javascript:showRanges(0);" onmouseout="javascript:hideRanges() -->
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnHard" name="spnHard" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnHard2" name="spnHard2" size="3" /></div>
											</div>
											<div class="row">
												<div class="col-md-4 column">Ciscenje</div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnCleansing" name="spnCleansing" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnCleansing2" name="spnCleansing2" size="3" /></div>
											</div>
											<div class="row">
												<div class="col-md-4 column">Kondicionir.</div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnConditioning" name="spnConditioning" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnConditioning2" name="spnConditioning2" size="3" /></div>
											</div>
											<div class="row">
												<div class="col-md-4 column">Penjenje</div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnBubbly" name="spnBubbly" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnBubbly2" name="spnBubbly2" size="3" /></div>
											</div>
											<div class="row">
												<div class="col-md-4 column">Kremast</div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnCreamy" name="spnCreamy" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnCreamy2" name="spnCreamy2" size="3" /></div>
											</div>
											<div class="row">
												<div class="col-md-4 column">Jod</div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnIodine" name="spnIodine" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnIodine2" name="spnIodine2" size="3" /></div>
											</div>
											<div class="row">
												<div class="col-md-4 column">INS</div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnINS" name="spnINS" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnINS2" name="spnINS2" size="3" /></div>
											</div>
											<div class="row">
												<div class="col-md-12 column">&nbsp;</div>
												<div class="row">
													<div class="col-md-4 column">Lauricna</div>
													<!-- <div class="col-md-1 column">i</div> -->
													<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnLauric" name="spnLauric" value="0" size="3" /></div>
													<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnLauric2" name="spnLauric" value="0"size="3" /></div>
												</div>
												<div class="row">
													<div class="col-md-4 column">Miristinska</div>
													<!-- <div class="col-md-1 column">i</div> -->
													<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnMyristic" name="spnMyristic" value="0" size="3" /></div>
													<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnMyristic2" name="spnMyristic2" value="0" size="3" /></div>
												</div>
												<div class="row">
												<div class="col-md-4 column">Palmitinska</div>
												<!-- <div class="col-md-1 column">i</div> -->
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnPalmitic" name="spnPalmitic" value="0" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnPalmitic2" name="spnPalmitic2" value="0" size="3" /></div>
												</div>
												<div class="row">
												<div class="col-md-4 column">Stearinska</div>
												<!-- <div class="col-md-1 column">i</div> -->
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnStearic" name="spnStearic" value="0" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnStearic2" name="spnStearic2" value="0" size="3" /></div>
												</div>
												<div class="row">
												<div class="col-md-4 column">Ricinoleicna</div>
												<!-- <div class="col-md-1 column">i</div> -->
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnRicinoleic" name="spnRicinoleic" value="0" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnRicinoleic2" name="spnRicinoleic2" value="0" size="3" /></div>
												</div>
												<div class="row">
												<div class="col-md-4 column">Oleicna</div>
												<!-- <div class="col-md-1 column">i</div> -->
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnOleic" name="spnOleic" value="0" size="3"  /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnOleic2" name="spnOleic2" value="0" size="3" /></div>
												</div>
												<div class="row">
												<div class="col-md-4 column">Linoleicna</div>
												<!-- <div class="col-md-1 column">i</div> -->
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnLinoleic" name="spnLinoleic" value="0" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnLinoleic2" name="spnLinoleic2" value="0"size="3"  /></div>
												</div>
												<div class="row">
												<div class="col-md-4 column">Linolenicna</div>
												<!-- <div class="col-md-1 column">i</div> -->
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA" type="text" id="spnLinolenic" name="spnLinolenic" value="0" size="3" /></div>
												<div class="col-md-4 column"><input readonly="readonly" class="txtSQ_FA2" type="text" id="spnLinolenic2" name="spnLinolenic2" value="0" size="3" /></div>
												</div>
											</div>
											<div class="row">&nbsp</div>
											<div class="row">
												Zas : Nezas &nbsp;&nbsp;&nbsp;&nbsp; <input style="width:75px;background-color:#FFFFFF;padding-left:4px" id="tdSatRatioMain" name="tdSatRatioMain"  readonly="readonly"  type="text" value="" />
											</div>
										</div>
										<div class="col-md-6 column">
										<div class="row">Ulja, masti i voskovi</div>
										<div class="row">&nbsp</div>
										<div class="row">
											<select name="selOil" id="selOil" size="30" onclick="selOilOnChange()" ondblclick="autoAddOil()" style="max-width:100%;">
											<?php 
												
												global $wpdb;
												$ingredients_query = 'select * from ' . $wpdb->get_blog_prefix();
												$ingredients_query .= 'sapunkalk ';
												//$ingredients_query .= 'ORDER by bug_id DESC';
												
												$ingredients = $wpdb->get_results( $wpdb->prepare( $ingredients_query ),ARRAY_A );
												
												foreach ( $ingredients as $ingredient ) {
											?>
													<option title="<?php echo $ingredient['sirovina_naziv']; ?> "value="<?php 
													echo $ingredient['temp_id'] .','. 
														$ingredient['sap'] .','.
														$ingredient['iodine'] .','.
														$ingredient['ins'] .','.
														$ingredient['lauric'] .','.
														$ingredient['myristic'] .','.
														$ingredient['palmitic'] .','.
														$ingredient['stearic'] .','.
														$ingredient['ricinoleic'] .','.
														$ingredient['oleic'] .','.
														$ingredient['linoleic'] .','.
														$ingredient['linolenic'];
													
													?>"><?php echo $ingredient['sirovina_naziv']; ?></option>
											<?php 
											}
											
											?>
												
											</select>
										</div>
										<div class="row">&nbsp;</div>
										<div class="col-md-6 column">
											<div class="col-md-6 column">NaOH SAP</div>
											<div class="col-md-6 column"><input type="text" readonly="readonly" name="txtSAPNaOH" id="txtSAPNaOH" class="inputRRO" value="0.134" size="4" /></div>
										</div>
										<div class="col-md-6 column">
											<div class="col-md-6 column">KOH SAP</div>
											<div class="col-md-6 column"><input type="text" readonly="readonly" name="txtSAPKOH" id="txtSAPKOH"  class="inputRRO" value="0.188" size="4"/></div>
										</div>
										</div>
									</div>
								</div>
								<div class="col-md-7 column">
									<div class="col-md-12 column"> <!-- recipe row -->
										<select id="selRecipeName" name="selRecipeName" style="width:auto;">
											<?php 
												for ($i = 1; $i<=8; $i++){
											?>
													<option value="Recipe<?php echo $i;?>w">Recept <?php echo $i;?></option>
											<?php 
												}
											?>
										</select>&nbsp;
										<input type="button" id="btnSaveRecipe" style="width:100px;cursor:default; font-size:12px; text-align:left;" disabled="disabled" onclick="javascript:SaveRecipe()" value="Snimi recept" />
										<input type="button" style="width:100px;cursor:pointer; font-size:12px; text-align:left;" onclick="javascript:GetRecipe()" value="Ucitaj recept" />
									</div>
									<div class="col-md-12 column"> <!-- empty row -->
										&nbsp
									</div>
									<div class="row">
									<div class="col-md-12 column"> <!-- input boxes row -->
										<div class="col-md-2 column">&nbsp;</div>
										<div class="col-md-6 column">
											<input type="button" value="Dodaj" style="font-size:11px; text-align:left;" onclick="javascript:autoAddOil();" />
											<!--<button type="button" class="btn btn-default" value="Add" onclick="javascript:autoAddOil();" />-->
											<input name="btnRemoveNumber" type="button" value="Ukloni #" style="font-size:11px; text-align:left;" onclick="javascript:removeOilTablet();" />
											<input id="txtRemoveItem" type="text" size="1" maxlength="2" style="font-size:12px; text-align:center;background-color:#C9FCD0;" />
										</div>
										<div class="col-md-2 column">
											<input class="RecipePercent"  type="radio" id="radPercent0" name="R3" value="0" checked="checked"  onclick="PorWClick()" /><br /><label id="lblPC" style="font-weight:900;">%</label>
										</div>
										<div class="col-md-2 column">
											<input class="RecipeWeight" type="radio" id="radPercent1" name="R3" value="1" onclick="PorWClick()" /><br /><label id="lblWT" style="font-weight:900;">lb</label>
										</div>
										
										<?php 
												$tab_index = 15;
												for ($i = 1; $i<=14; $i++){
											?>

										<div class="col-md-2 column">&nbsp;<a href="javascript:addOil(<?php echo $i-1;?>)">&#43;</a>&nbsp; <a href="javascript:removeOil(<?php echo $i-1;?>)" style="font-size:16px;">&#45;</a> <?php echo $i;?>
										</div>
										<div class="col-md-6 column">
											<input type="text" name="txtOil<?php echo $i;?>" id="txtOil<?php echo $i;?>" value="" readonly="readonly" class="RecipeOilNameInput" size="35"/>
										</div>
										<div class="col-md-2 column">
											<input type="text" name="txtPerCent<?php echo $i;?>" id="txtPerCent<?php echo $i;?>" class="RecipePercent" style="background-color:#C9FCD0;" tabindex="<?php echo $i;?>" onchange="javascript:textChanged();" value="" size="5"/>
										</div>
										<div class="col-md-2 column">
											<input type="text" name="txtWeight<?php echo $i;?>" id="txtWeight<?php echo $i;?>" readonly="readonly" tabindex="15" onchange="javascript:textChanged();"  class="RecipeWeight" value="" size="5" />
										</div>
										<?php 
												}
											?>
										<div class="col-md-2 column">&nbsp;</div>
										<div class="col-md-6 column">Totals:</div>
										<div class="col-md-2 column"><input class="RecipePercent" style="background-color:#FFFFEE;" value="" readonly="readonly" type="text" id="txtTotalPerCent" size="5"/></div>
										<div class="col-md-2 column"><input class="RecipeWeight" style="background-color:#FFFFEE;" value="" readonly="readonly" type="text" id="txtTotalRecipeWeight" size="5"/></div>
										
									</div>
								</div>	
									<div class="col-md-12 column">&nbsp;</div>
									<div class="col-md-12 column"> <!-- buttons1 row -->
										<div class="row">
											<div class="col-md-6 column" style="align:right">
												<input type="button" style="cursor:pointer;width:125px;" onclick="javascript:computeRecipe(0)" value="1. Izracunaj" title="Click to calculate recipe, then click View/Print Recipe." />
											</div>
											<div class="col-md-6 column">
												<input type="button" onclick="javascript:clearAll(true)" value="Resetuj SVE" title="Resets all values to default state." style="background:#F00;color:#FFFFFF; font-weight:bold" />
											</div>
										</div>
									</div>
									<div class="col-md-12 column">&nbsp;</div>
									<div class="col-md-12 column"> <!-- buttons2 boxes row -->
										<input type="button" id="btnViewRecipe" disabled="disabled" onclick="javascript:printRecipe()" value="2. View or Print Recipe" style="width:145px;" title="Click to see lye and water amounts and other recipe details." />&nbsp;<input id="chkHowToPrint" name="chkHowToPrint" type="checkbox" title="Open a new window or tab each time you click 'View/Print Recipe'." /><label for="chkHowToPrint" title="Open a new window or tab each time you click 'View/Print Recipe'.">Multiple tabs</label>&nbsp;<input id="chkBold" name="chkBold" type="checkbox" title="Water, lye and individual oil weights are in bold typeface."/><label for="chkBold" title="Water, lye and individual oil weights are in bold typeface.">Bold</label>
										<!--<div class="col-md-4 column">Totals:</div>
										<div class="col-md-4 column">Totals:</div>
										<div class="col-md-4 column">Totals:</div> -->
									</div>
								</div>
							</div>
					</div>
				</div>	
				
				
				
				
				<div id="divRanges"></div>
				<div id="divFADetail"></div>

				<input type="hidden" id="hdnTWOils" value="" />
					<input type="hidden" id="hdnPorW" value="0" />
					<input type="hidden" name="hdnValue0" id="hdnValue0" value="-1" />
					<input type="hidden" name="hdnValue1" id="hdnValue1" value="-1" />
					<input type="hidden" name="hdnValue2" id="hdnValue2" value="-1" />
					<input type="hidden" name="hdnValue3" id="hdnValue3" value="-1" />
					<input type="hidden" name="hdnValue4" id="hdnValue4" value="-1" />
					<input type="hidden" name="hdnValue5" id="hdnValue5" value="-1" />
					<input type="hidden" name="hdnValue6" id="hdnValue6" value="-1" />
					<input type="hidden" name="hdnValue7" id="hdnValue7" value="-1" />
					<input type="hidden" name="hdnValue8" id="hdnValue8" value="-1" />
					<input type="hidden" name="hdnValue9" id="hdnValue9" value="-1" />
					<input type="hidden" name="hdnValue10" id="hdnValue10" value="-1" />
					<input type="hidden" name="hdnValue11" id="hdnValue11" value="-1" />
					<input type="hidden" name="hdnValue12" id="hdnValue12" value="-1" />
					<input type="hidden" name="hdnValue13" id="hdnValue13" value="-1" />
					<input type="hidden" name="hdnSap0" id="hdnSap0" value="-1" />
					<input type="hidden" name="hdnSap1" id="hdnSap1" value="-1" />
					<input type="hidden" name="hdnSap2" id="hdnSap2" value="-1" />
					<input type="hidden" name="hdnSap3" id="hdnSap3" value="-1" />
					<input type="hidden" name="hdnSap4" id="hdnSap4" value="-1" />
					<input type="hidden" name="hdnSap5" id="hdnSap5" value="-1" />
					<input type="hidden" name="hdnSap6" id="hdnSap6" value="-1" />
					<input type="hidden" name="hdnSap7" id="hdnSap7" value="-1" />
					<input type="hidden" name="hdnSap8" id="hdnSap8" value="-1" />
					<input type="hidden" name="hdnSap9" id="hdnSap9" value="-1" />
					<input type="hidden" name="hdnSap10" id="hdnSap10" value="-1" />
					<input type="hidden" name="hdnSap11" id="hdnSap11" value="-1" />
					<input type="hidden" name="hdnSap12" id="hdnSap12" value="-1" />
					<input type="hidden" name="hdnSap13" id="hdnSap13" value="-1" />
					<input type="hidden" name="hdnIndex0" id="hdnIndex0" value="-1" />
					<input type="hidden" name="hdnIndex1" id="hdnIndex1" value="-1" />
					<input type="hidden" name="hdnIndex2" id="hdnIndex2" value="-1" />
					<input type="hidden" name="hdnIndex3" id="hdnIndex3" value="-1" />
					<input type="hidden" name="hdnIndex4" id="hdnIndex4" value="-1" />
					<input type="hidden" name="hdnIndex5" id="hdnIndex5" value="-1" />
					<input type="hidden" name="hdnIndex6" id="hdnIndex6" value="-1" />
					<input type="hidden" name="hdnIndex7" id="hdnIndex7" value="-1" />
					<input type="hidden" name="hdnIndex8" id="hdnIndex8" value="-1" />
					<input type="hidden" name="hdnIndex9" id="hdnIndex9" value="-1" />
					<input type="hidden" name="hdnIndex10" id="hdnIndex10" value="-1" />
					<input type="hidden" name="hdnIndex11" id="hdnIndex11" value="-1" />
					<input type="hidden" name="hdnIndex12" id="hdnIndex12" value="-1" />
					<input type="hidden" name="hdnIndex13" id="hdnIndex13" value="-1" />
					<input type="hidden" name="hdnHard2" id="hdnHard2" value="-1" />
					<input type="hidden" name="hdnCleansing2" id="hdnCleansing2" value="-1" />
					<input type="hidden" name="hdnConditioning2" id="hdnConditioning2" value="-1" />
					<input type="hidden" name="hdnBubbly2" id="hdnBubbly2" value="-1" />
					<input type="hidden" name="hdnCreamy2" id="hdnCreamy2" value="-1" />
					<input type="hidden" name="hdnIodine2" id="hdnIodine2" value="-1" />
					<input type="hidden" name="hdnINS2" id="hdnINS2" value="-1" />
					<input type="hidden" name="hdnLauric2" id="hdnLauric2" value="-1" />
					<input type="hidden" name="hdnMyristic2" id="hdnMyristic2" value="-1" />
					<input type="hidden" name="hdnPalmitic2" id="hdnPalmitic2" value="-1" />
					<input type="hidden" name="hdnStearic2" id="hdnStearic2" value="-1" />
					<input type="hidden" name="hdnRicinoleic2" id="hdnRicinoleic2" value="-1" />
					<input type="hidden" name="hdnOleic2" id="hdnOleic2" value="-1" />
					<input type="hidden" name="hdnLinoleic2" id="hdnLinoleic2" value="-1" />
					<input type="hidden" name="hdnLinolenic2" id="hdnLinolenic2" value="-1" /
				</form>
				<?php
			
	

}




?>