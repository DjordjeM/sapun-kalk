<?php

/*
Plugin Name: Sapunski kalkulator
Plugin URI:
Description: Sapunski kalkulator omogucava izracunavanje kolicine baze neophodne za saponifikaciju izabranih masti
Version: 1.0
Author: Djordje Marjanovic
Author URI: 
License: GPLv2
*/




function sapunkalk_activation() {
	// Get access to global database access class
	global $wpdb;

	// Create table on main blog in network mode or single blog
	sapunkalk_create_table( $wpdb->get_blog_prefix() );
	sapunkalk_populate_table( $wpdb->get_blog_prefix() );
}

function sapunkalk_create_table( $prefix ) {
	// Prepare SQL query to create database table
	// using function parameter
	$creation_query =
	'CREATE TABLE IF NOT EXISTS ' . $prefix . 'sapunkalk (
	`sirovina_id` int(20) NOT NULL AUTO_INCREMENT,
	`sirovina_naziv` text,
	`sap`  DECIMAL(4,3) DEFAULT 0, 
	`iodine` SMALLINT(4) DEFAULT 0,
	`ins` SMALLINT(4) DEFAULT 0,
	`lauric` SMALLINT(4) DEFAULT 0,
	`myristic` SMALLINT(4) DEFAULT 0,
	`palmitic` SMALLINT(4) DEFAULT 0,
	`stearic` SMALLINT(4) DEFAULT 0,
	`ricinoleic` SMALLINT(4) DEFAULT 0,
	`oleic` SMALLINT(4) DEFAULT 0,
	`linoleic` SMALLINT(4) DEFAULT 0,
	`linolenic` SMALLINT(4) DEFAULT 0,
	PRIMARY KEY (`sirovina_id`)
	);';
	global $wpdb;
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $creation_query );
	
	//$wpdb->query( $creation_query );
	}
	
	
function sapunkalk_populate_table	( $prefix ) {
	//Insert data
	$insert_query =
	'INSERT INTO '. $prefix . 'sapunkalk (temp_id, sirovina_naziv, sap, iodine, ins, lauric, myristic, palmitic, stearic, ricinoleic, oleic, linoleic, linolenic)
	SELECT 114, "Almond Butter", 0.188, 70, 118, 0, 1, 9, 15, 0, 58, 16, 0 
	UNION ALL 
	SELECT 1, "Almond Oil, sweet", 0.195, 99, 97, 0, 0, 7, 0, 0, 71, 18, 0 
	UNION ALL 
	SELECT 68, "Aloe Butter", 0.24, 9, 241, 45, 18, 8, 3, 0, 7, 2, 0 
	UNION ALL 
	SELECT 96, "Andiroba Oil,karaba,crabwood", 0.188, 68, 120, 0, 0, 28, 8, 0, 51, 9, 0 
	UNION ALL 
	SELECT 2, "Apricot Kernal Oil", 0.195, 100, 91, 0, 0, 6, 0, 0, 66, 27, 0 
	UNION ALL 
	SELECT 58, "Argan Oil", 0.191, 95, 95, 0, 1, 14, 0, 0, 46, 34, 1 
	UNION ALL 
	SELECT 53, "Avocado butter", 0.187, 67, 120, 0, 0, 21, 10, 0, 53, 6, 2 
	UNION ALL 
	SELECT 3, "Avocado Oil", 0.186, 86, 99, 0, 0, 20, 2, 0, 58, 12, 0 
	UNION ALL 
	SELECT 4, "Babassu Oil", 0.245, 15, 230, 50, 20, 11, 4, 0, 10, 0, 0 
	UNION ALL 
	SELECT 59, "Baobab Oil", 0.2, 75, 125, 0, 1, 24, 4, 0, 37, 28, 2 
	UNION ALL 
	SELECT 5, "Beeswax", 0.094, 10, 84, 0, 0, 0, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 6, "Black Cumin Seed Oil, nigella sativa", 0.195, 133, 62, 0, 0, 13, 3, 0, 22, 60, 1 
	UNION ALL 
	SELECT 136, "Black Current Seed Oil", 0.19, 178, 12, 0, 0, 6, 2, 0, 13, 46, 29 
	UNION ALL 
	SELECT 66, "Borage Oil", 0.19, 135, 55, 0, 0, 10, 4, 0, 20, 43, 5 
	UNION ALL 
	SELECT 138, "Broccoli Seed Oil, Brassica Oleracea", 0.172, 105, 67, 0, 0, 3, 1, 0, 14, 11, 9 
	UNION ALL 
	SELECT 60, "Camelina Seed Oil", 0.188, 144, 44, 0, 0, 6, 2, 0, 24, 19, 45 
	UNION ALL 
	SELECT 80, "Camellia Oil, Tea Seed", 0.195, 85, 110, 0, 0, 9, 2, 0, 77, 8, 0 
	UNION ALL 
	SELECT 142, "Candelilla Wax", 0.044, 32, 12, 0, 0, 0, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 7, "Canola Oil", 0.186, 110, 56, 0, 0, 4, 2, 0, 61, 21, 9 
	UNION ALL 
	SELECT 84, "Canola Oil, high oleic", 0.186, 96, 90, 0, 0, 4, 2, 0, 74, 12, 4 
	UNION ALL 
	SELECT 144, "Carrot Seed Oil, cold pressed", 0.144, 56, 0, 0, 0, 4, 0, 0, 80, 13, 0 
	UNION ALL 
	SELECT 8, "Castor Oil", 0.18, 86, 95, 0, 0, 0, 0, 90, 4, 4, 0 
	UNION ALL 
	SELECT 79, "Cherry Kern1 Oil, p. avium", 0.19, 128, 62, 0, 0, 8, 3, 0, 31, 45, 11 
	UNION ALL 
	SELECT 90, "Cherry Kern2 Oil, p. cesarus", 0.192, 118, 74, 0, 0, 6, 3, 0, 50, 40, 0 
	UNION ALL 
	SELECT 56, "Chicken Fat", 0.195, 69, 130, 0, 1, 25, 7, 0, 38, 21, 0 
	UNION ALL 
	SELECT 9, "Cocoa Butter", 0.194, 37, 157, 0, 0, 28, 33, 0, 35, 3, 0 
	UNION ALL 
	SELECT 90, "Coconut Oil, 76 deg", 0.257, 10, 258, 48, 19, 9, 3, 0, 8, 2, 0 
	UNION ALL 
	SELECT 72, "Coconut Oil, 92 deg", 0.257, 3, 258, 48, 19, 9, 3, 0, 8, 2, 0 
	UNION ALL 
	SELECT 65, "Coconut Oil, fractionated", 0.325, 1, 324, 2, 1, 0, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 93, "Coffee Bean Oil, green", 0.185, 85, 100, 0, 0, 38, 8, 0, 9, 39, 2 
	UNION ALL 
	SELECT 74, "Coffee Bean Oil, roasted", 0.18, 87, 93, 0, 0, 40, 0, 0, 8, 38, 2 
	UNION ALL 
	SELECT 102, "Cohune Oil", 0.205, 30, 175, 51, 13, 8, 3, 0, 18, 3, 0 
	UNION ALL 
	SELECT 11, "Corn Oil", 0.192, 117, 69, 0, 0, 12, 2, 0, 32, 51, 1 
	UNION ALL 
	SELECT 12, "Cottonseed Oil", 0.194, 108, 89, 0, 0, 13, 13, 0, 18, 52, 1 
	UNION ALL 
	SELECT 92, "Cranberry Seed Oil", 0.19, 150, 40, 0, 0, 6, 2, 0, 23, 37, 32 
	UNION ALL 
	SELECT 104, "Crisco, new w/palm", 0.193, 111, 82, 0, 0, 20, 5, 0, 28, 40, 6 
	UNION ALL 
	SELECT 13, "Crisco, old", 0.192, 93, 115, 0, 0, 13, 13, 0, 18, 52, 0 
	UNION ALL 
	SELECT101,  "Cupuacu Butter", 0.193, 38, 155, 0, 0, 8, 34, 0, 41, 3, 0 
	UNION ALL 
	SELECT 87, "Duck Fat, flesh and skin", 0.194, 72, 122, 0, 1, 26, 9, 0, 44, 13, 1 
	UNION ALL 
	SELECT 14, "Emu Oil", 0.19, 60, 128, 0, 0, 23, 9, 0, 47, 8, 0 
	UNION ALL 
	SELECT 15, "Evening Primrose Oil", 0.19, 160, 30, 0, 0, 0, 0, 0, 0, 80, 9 
	UNION ALL 
	SELECT 16, "Flax Oil, linseed", 0.19, 180, -6, 0, 0, 6, 3, 0, 27, 13, 50 
	UNION ALL 
	SELECT 95, "Ghee, any bovine", 0.227, 30, 191, 4, 11, 28, 12, 0, 19, 2, 1 
	UNION ALL 
	SELECT 17, "Goose Fat", 0.192, 65, 130, 0, 0, 21, 6, 0, 54, 10, 0 
	UNION ALL 
	SELECT 18, "Grapeseed Oil", 0.181, 131, 66, 0, 0, 8, 4, 0, 20, 68, 0 
	UNION ALL 
	SELECT 19, "Hazelnut Oil", 0.195, 97, 94, 0, 0, 5, 3, 0, 75, 10, 0 
	UNION ALL 
	SELECT 20, "Hemp Oil", 0.193, 165, 39, 0, 0, 6, 2, 0, 12, 57, 21 
	UNION ALL 
	SELECT 94, "Horse Oil", 0.196, 79, 117, 0, 3, 26, 5, 0, 10, 20, 19 
	UNION ALL 
	SELECT 62, "Illipe Butter", 0.185, 33, 152, 0, 0, 17, 45, 0, 35, 0, 0 
	UNION ALL 
	SELECT 143, "Japan Wax", 0.215, 11, 204, 0, 1, 80, 7, 0, 4, 0, 0 
	UNION ALL 
	SELECT 108, "Jatropha Oil, soapnut seed oil", 0.193, 102, 91, 0, 0, 9, 7, 0, 44, 34, 0 
	UNION ALL 
	SELECT 21, "Jojoba Oil (a Liquid Wax Ester)", 0.092, 83, 11, 0, 0, 0, 0, 0, 12, 0, 0 
	UNION ALL 
	SELECT 51, "Karanja Oil", 0.183, 85, 98, 0, 0, 6, 6, 0, 58, 15, 0 
	UNION ALL 
	SELECT 23, "Kokum Butter", 0.19, 35, 155, 0, 0, 4, 56, 0, 36, 1, 0 
	UNION ALL 
	SELECT 86, "Kpangnan Butter", 0.191, 42, 149, 0, 0, 6, 44, 0, 49, 1, 0 
	UNION ALL 
	SELECT 24, "Kukui nut Oil", 0.189, 168, 24, 0, 0, 6, 2, 0, 20, 42, 29 
	UNION ALL 
	SELECT 25, "Lanolin liquid Wax", 0.106, 27, 83, 0, 0, 0, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 26, "Lard, Pig Tallow (Manteca)", 0.198, 57, 139, 0, 1, 28, 13, 0, 46, 6, 0 
	UNION ALL 
	SELECT 127, "Laurel Fruit Oil", 0.198, 74, 124, 25, 1, 15, 1, 0, 31, 26, 1 
	UNION ALL 
	SELECT 125, "Lauric Acid", 0.28, 0, 280, 99, 1, 0, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 27, "Linseed Oil, flax", 0.19, 180, -6, 0, 0, 6, 3, 0, 27, 13, 50 
	UNION ALL 
	SELECT 119, "Loofa Seed Oil, Luffa cylinderica", 0.187, 108, 79, 0, 0, 9, 18, 0, 30, 47, 0 
	UNION ALL 
	SELECT 124, "Macadamia Nut Butter", 0.188, 70, 118, 0, 1, 6, 12, 0, 56, 3, 1 
	UNION ALL 
	SELECT 28, "Macadamia Nut Oil", 0.195, 76, 119, 0, 0, 9, 5, 0, 59, 2, 0 
	UNION ALL 
	SELECT 141, "Mafura Butter, Trichilia emetica ", 0.198, 66, 132, 0, 1, 37, 3, 0, 49, 11, 1 
	UNION ALL 
	SELECT 29, "Mango Seed Butter", 0.191, 45, 146, 0, 0, 7, 42, 0, 45, 3, 0 
	UNION ALL 
	SELECT 30, "Mango Seed Oil", 0.19, 60, 130, 0, 0, 8, 27, 0, 52, 8, 1 
	UNION ALL 
	SELECT 99, "Marula Oil", 0.192, 73, 119, 0, 0, 11, 7, 0, 75, 4, 0 
	UNION ALL 
	SELECT 31, "Meadowfoam Oil", 0.169, 92, 77, 0, 0, 0, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 32, "Milk Fat, any bovine", 0.227, 30, 191, 4, 11, 28, 12, 0, 19, 2, 1 
	UNION ALL 
	SELECT 137, "Milk Thistle Oil", 0.196, 115, 81, 0, 0, 7, 2, 0, 26, 64, 0 
	UNION ALL 
	SELECT 67, "Mink Oil", 0.196, 55, 141, 0, 0, 0, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 69, "Monoi de Tahiti  Oil", 0.255, 9, 246, 44, 16, 10, 3, 0, 0, 2, 0 
	UNION ALL 
	SELECT 109, "Moringa Oil", 0.192, 68, 124, 0, 0, 7, 7, 0, 71, 2, 0 
	UNION ALL 
	SELECT 63, "Mowrah Butter", 0.194, 62, 132, 0, 0, 24, 22, 0, 36, 15, 0 
	UNION ALL 
	SELECT 106, "Murumuru Butter", 0.275, 25, 250, 47, 26, 6, 3, 0, 15, 3, 0 
	UNION ALL 
	SELECT 103, "Mustard Oil, kachi ghani", 0.173, 101, 72, 0, 0, 2, 2, 0, 18, 14, 9 
	UNION ALL 
	SELECT 76, "Myristic Acid", 0.247, 1, 246, 0, 99, 0, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 121, "Neatsfoot Oil", 0.18, 90, 90, 0, 0, 0, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 33, "Neem Tree Oil", 0.195, 89, 124, 0, 0, 18, 15, 0, 50, 13, 0 
	UNION ALL 
	SELECT 139, "Nutmeg Butter", 0.1624, 46, 116, 3, 83, 4, 0, 0, 5, 0, 0 
	UNION ALL 
	SELECT 117, "Oat Oil", 0.19, 104, 86, 0, 0, 15, 2, 0, 40, 39, 0 
	UNION ALL 
	SELECT 88, "Oleic Acid", 0.202, 92, 110, 0, 0, 0, 0, 0, 99, 0, 0 
	UNION ALL 
	SELECT 34, "Olive Oil", 0.19, 85, 105, 0, 0, 14, 3, 0, 69, 12, 1 
	UNION ALL 
	SELECT 52, "Olive Oil  pomace", 0.188, 84, 104, 0, 0, 14, 3, 0, 69, 12, 2 
	UNION ALL 
	SELECT 82, "Ostrich Oil", 0.1946, 97, 128, 3, 1, 26, 6, 0, 37, 17, 3 
	UNION ALL 
	SELECT 35, "Palm Kernel Oil", 0.247, 20, 227, 49, 16, 8, 2, 0, 15, 3, 0 
	UNION ALL 
	SELECT 126, "Palm Kernel Oil Flakes, hydrogenated", 0.247, 20, 227, 49, 17, 8, 16, 0, 4, 0, 0 
	UNION ALL 
	SELECT 36, "Palm Oil", 0.199, 53, 145, 0, 1, 44, 5, 0, 39, 10, 0 
	UNION ALL 
	SELECT 113, "Palm Stearin", 0.199, 48, 151, 0, 2, 60, 5, 0, 26, 7, 0 
	UNION ALL 
	SELECT 77, "Palmitic Acid", 0.215, 2, 213, 0, 0, 98, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 131, "Palmolein", 0.2, 58, 142, 0, 1, 40, 5, 0, 43, 11, 0 
	UNION ALL 
	SELECT 120, "Papaya seed oil, Carica papaya", 0.158, 67, 91, 0, 0, 13, 5, 0, 76, 3, 0 
	UNION ALL 
	SELECT 37, "Passion Fruit Seed Oil", 0.183, 136, 47, 0, 0, 10, 3, 0, 15, 70, 1 
	UNION ALL 
	SELECT 70, "Peach Kernel Oil", 0.191, 108, 87, 0, 0, 6, 2, 0, 65, 25, 1 
	UNION ALL 
	SELECT 38, "Peanut Oil", 0.192, 92, 99, 0, 0, 8, 3, 0, 56, 26, 0 
	UNION ALL 
	SELECT 137, "Pecan Oil", 0.19, 113, 77, 0, 0, 7, 2, 0, 50, 39, 2 
	UNION ALL 
	SELECT 75, "Perilla Seed Oil", 0.19, 196, -6, 0, 0, 6, 2, 0, 15, 16, 56 
	UNION ALL 
	SELECT 85, "Pine Tar, lye calc only no FA", 0.06, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 
	UNION ALL 
	SELECT 39, "Pistachio Oil", 0.186, 95, 92, 0, 0, 11, 1, 0, 63, 25, 0 
	UNION ALL 
	SELECT 107, "Plum Kernel Oil", 0.194, 98, 96, 0, 0, 3, 0, 0, 68, 23, 0 
	UNION ALL 
	SELECT 128, "Pomegranate Seed Oil", 0.19, 22, 168, 0, 0, 3, 3, 0, 7, 7, 78 
	UNION ALL 
	SELECT 73, "Poppy Seed Oil", 0.194, 140, 54, 0, 0, 10, 2, 0, 17, 69, 2 
	UNION ALL 
	SELECT 83, "Pumpkin Seed Oil virgin", 0.195, 128, 67, 0, 0, 11, 8, 0, 33, 50, 0 
	UNION ALL 
	SELECT 91, "Rabbit Fat", 0.201, 85, 116, 0, 3, 30, 6, 0, 30, 20, 5 
	UNION ALL 
	SELECT 40, "Rapeseed Oil, unrefined canola", 0.175, 106, 69, 0, 0, 4, 1, 0, 17, 13, 9 
	UNION ALL 
	SELECT 129, "Raspberry Seed Oil", 0.187, 163, 24, 0, 0, 3, 0, 0, 13, 55, 26 
	UNION ALL 
	SELECT 89, "Red Palm Butter", 0.199, 53, 145, 0, 1, 44, 5, 0, 39, 10, 0 
	UNION ALL 
	SELECT 41, "Rice Bran Oil", 0.179, 110, 70, 0, 1, 22, 3, 0, 43, 26, 0 
	UNION ALL 
	SELECT 61, "Rosehip Oil", 0.187, 188, 10, 0, 0, 4, 2, 0, 12, 46, 31 
	UNION ALL 
	SELECT 122, "Sacha Inchi, Plukenetia volubilis", 0.188, 141, 47, 0, 0, 4, 3, 0, 10, 35, 48 
	UNION ALL 
	SELECT 42, "Safflower Oil", 0.192, 145, 47, 0, 0, 7, 0, 0, 15, 75, 0 
	UNION ALL 
	SELECT 78, "Safflower Oil, high oleic", 0.19, 93, 97, 0, 0, 5, 2, 0, 77, 15, 0 
	UNION ALL 
	SELECT 64, "Sal Butter", 0.185, 39, 146, 0, 0, 6, 44, 0, 40, 2, 0 
	UNION ALL 
	SELECT 140, "Salmon Oil", 0.185, 169, 16, 0, 5, 19, 2, 0, 23, 2, 1 
	UNION ALL 
	SELECT 111, "Saw Palmetto Extract", 0.23, 45, 185, 29, 11, 8, 2, 0, 35, 4, 1 
	UNION ALL 
	SELECT 110, "Saw Palmetto Oil", 0.22, 44, 176, 29, 13, 9, 2, 0, 31, 4, 1 
	UNION ALL 
	SELECT 116, "Sea Buckthorn Oil, seed", 0.195, 165, 30, 0, 0, 7, 3, 0, 14, 36, 38 
	UNION ALL 
	SELECT 115, "Sea Buckthorn Oil, seed and berry", 0.183, 86, 97, 0, 0, 30, 1, 0, 28, 10, 0 
	UNION ALL 
	SELECT 43, "Sesame Oil", 0.188, 110, 81, 0, 0, 10, 5, 0, 40, 43, 0 
	UNION ALL 
	SELECT 44, "Shea Butter", 0.179, 59, 116, 0, 0, 5, 40, 0, 48, 6, 0 
	UNION ALL 
	SELECT 22, "Shea Oil, fractionated", 0.185, 83, 102, 0, 0, 6, 10, 0, 73, 11, 0 
	UNION ALL 
	SELECT 133, "SoapQuick, conventional", 0.212, 59, 153, 13, 6, 17, 3, 5, 42, 8, 1 
	UNION ALL 
	SELECT 134, "SoapQuick, organic", 0.213, 56, 156, 13, 5, 20, 3, 0, 45, 10, 0 
	UNION ALL 
	SELECT 45, "Soybean Oil", 0.191, 131, 61, 0, 0, 11, 5, 0, 24, 50, 8 
	UNION ALL 
	SELECT 81, "Soybean, 27.5% hydrogenated", 0.191, 78, 113, 0, 0, 9, 15, 0, 41, 7, 1 
	UNION ALL 
	SELECT 132, "Soybean, fully hydrogenated (soy wax)", 0.192, 1, 191, 0, 0, 11, 87, 0, 0, 0, 0 
	UNION ALL 
	SELECT 46, "Stearic Acid", 0.198, 2, 196, 0, 0, 0, 99, 0, 0, 0, 0 
	UNION ALL 
	SELECT 47, "Sunflower Oil", 0.189, 133, 63, 0, 0, 7, 4, 0, 16, 70, 1 
	UNION ALL 
	SELECT 71, "Sunflower Oil, high oleic", 0.189, 83, 106, 0, 0, 3, 4, 0, 83, 4, 1 
	UNION ALL 
	SELECT 112, "Tallow Bear", 0.1946, 92, 100, 0, 2, 7, 3, 0, 70, 9, 0 
	UNION ALL 
	SELECT 48, "Tallow Beef", 0.2, 45, 147, 2, 6, 28, 22, 0, 36, 3, 1 
	UNION ALL 
	SELECT 54, "Tallow Deer", 0.193, 31, 166, 0, 1, 20, 24, 0, 30, 15, 3 
	UNION ALL 
	SELECT 123, "Tallow Goat", 0.192, 40, 152, 5, 11, 23, 30, 0, 29, 2, 0 
	UNION ALL 
	SELECT 55, "Tallow Sheep", 0.194, 54, 156, 4, 10, 24, 13, 0, 26, 5, 0 
	UNION ALL 
	SELECT 57, "Tamanu Oil, kamani", 0.208, 111, 82, 0, 0, 12, 13, 0, 34, 38, 1 
	UNION ALL 
	SELECT 97, "Tucuma Seed Butter", 0.238, 13, 175, 48, 23, 6, 0, 0, 13, 0, 0 
	UNION ALL 
	SELECT 100, "Ucuuba Butter", 0.205, 38, 167, 0, 0, 0, 31, 0, 44, 5, 0 
	UNION ALL 
	SELECT 105, "Walmart GV Shortening, beef tallow, palm", 0.198, 49, 151, 1, 4, 35, 14, 0, 37, 6, 1 
	UNION ALL 
	SELECT 49, "Walnut Oil", 0.189, 145, 45, 0, 0, 7, 2, 0, 18, 60, 0 
	UNION ALL 
	SELECT 135, "Watermelon Seed Oil", 0.19, 119, 71, 0, 0, 11, 10, 0, 18, 60, 1 
	UNION ALL 
	SELECT 50, "Wheat Germ Oil", 0.183, 128, 58, 0, 0, 17, 2, 0, 17, 58, 0 
	UNION ALL 
	SELECT 98, "Yangu, cape chestnut", 0.192, 95, 97, 0, 0, 18, 5, 0, 45, 30, 1 
	UNION ALL 
	SELECT 118, "Zapote seed oil, (Aceite de Sapuyul or Mamey)", 0.188, 72, 116, 0, 0, 9, 21, 0, 52, 13, 0
	;';
	global $wpdb;
	$wpdb->query( $insert_query );
	}

register_activation_hook( __FILE__, 'sapunkalk_activation' );

	
if ( is_admin() ) {
	require plugin_dir_path( __FILE__ ) . 'admin.php';
}

function sapunkalk_page_header_output() {
	$sapunkalkjs_url = plugins_url() . '/sapun-kalk/js/sapun-kalk.js';
	wp_enqueue_script('sapunkal_js', $sapunkalkjs_url);
	wp_enqueue_style('twitter_bootstrap', plugins_url() . '/sapun-kalk/css/bootstrap.min.css');
	 }
	
//add_action( 'wp_head', 'sapunkalk_page_header_output' );
add_action( 'wp_enqueue_scripts', 'sapunkalk_page_header_output' );

require_once plugin_dir_path( __FILE__ ) . 'sapunkalk_render.php';
?>