var Hard = 0,
    Cleansing = 0,
    Bubbly = 0,
    Conditioning = 0,
    Creamy = 0,
    Iodine = 0,
    INS = 0,
    Lauric = 0,
    Linoleic = 0,
    Linolenic = 0,
    Myristic = 0,
    Oleic = 0,
    Palmitic = 0,
    Ricinoleic = 0,
    Stearic = 0,
    Saturated = 0,
    MonoUnsaturated = 0,
    PolyUnsaturated = 0,
    ary14RecipeWeightVals = Array(14),
    ary14RecipeSapVals = Array(14),
    ary14RecipeSelIndexes = Array(14),
    ary14RecipePercentVals = Array(14),
    ary14SelectedOilIds = Array(14),
    g_radio, g_tot = 0,
    g_Computed = !1,
    g_90PercentKOH, unit = "lb",
    FixedConc, FixedRatio, helpImg = new Image,
    aryRanges = Array(7),
    sRangeTrailer = "<br /><br /><span style='font-size:12px;font-style:italic'>These ranges for are designed as guidelines. With experience, you may want to venture above or below these limits.</span>";
aryRanges[0] = "<span style='font-weight:bold'>Hardness</span> (in the All column): 29 to 54, higher is harder." + sRangeTrailer;
aryRanges[1] = "<span style='font-weight:bold'>Cleansing</span> (in the All column): 12 to 22, all soap cleanses but the higher numbers more so." + sRangeTrailer;
aryRanges[2] = "<span style='font-weight:bold'>Conditioning</span> (in the All column): 44 to 69, higher offers more skin conditioning." + sRangeTrailer;
aryRanges[3] = "<span style='font-weight:bold'>Bubbly</span> (in the All column): 14 to 46, higher gives a more bubbly lather." + sRangeTrailer;
aryRanges[4] = "<span style='font-weight:bold'>Creamy</span> (in the All column): 16 to 48, higher gives a more creamy lather." + sRangeTrailer;
aryRanges[5] = "<span style='font-weight:bold'>Iodine Value</span> (in the All column): 41 to 70, lower equals harder bar, much above 70 and bar could possibly be too soft." + sRangeTrailer;
aryRanges[6] = "<span style='font-weight:bold'>INS</span> (in the All column): 136 to 165, the commonly accepted ideal is 160.&nbsp; This value gauges the physical qualities of the bar; primarily hardness, with higher being harder." + sRangeTrailer;
var aryFADetail = Array(8),
    tailFADetail = "<br /><br />Click on green 'i' button to get more detail from Wikipedia.";
aryFADetail[0] = "<b>Lauric Acid</b><br /><br />&bull; 12:0<br />&bull; Saturated<br />&bull; dodecanoic acid. &nbsp;&nbsp;(doe-dec-a-no-ik)";
aryFADetail[1] = "<b>Myristic Acid</b><br /><br />&bull; 14:0<br />&bull; Saturated<br />&bull; tetradecanoic acid. &nbsp;&nbsp;(te-tra-dec-a-no-ik)";
aryFADetail[2] = "<b>Palmitic Acid</b><br /><br />&bull; 16:0<br />&bull; Saturated<br />&bull; hexadecanoic acid. &nbsp;&nbsp;(hex-a-dec-a-no-ik)";
aryFADetail[3] = "<b>Stearic Acid</b><br /><br />&bull; 18:0<br />&bull; Saturated<br />&bull; octadecanoic acid. &nbsp;&nbsp;(oc-ta-dec-a-no-ik)";
aryFADetail[4] = "<b>Ricinoleic Acid</b><br /><br />&bull; 18:1<br />&bull; MonoUnsaturated<br />&bull; 12-hydroxy-9-cis-octadecenoic acid.<br />(hy-drox-y, sis, oc-ta-dec-e-no-ik)";
aryFADetail[5] = "<b>Oleic Acid</b><br /><br />&bull; 18:1<br />&bull; MonoUnsaturated<br />&bull; cis-9-octadecenoic acid. &nbsp;&nbsp;(sis, oc-ta-dec-e-no-ik)";
aryFADetail[6] = "<b>Linoleic Acid</b><br /><br />&bull; 18:2<br />&bull; PolyUnsaturated<br />&bull; cis, cis-9,12-octadecadienoic acid.<br /> (sis, oc-ta-dec-a-di-e-no-ic)";
aryFADetail[7] = "<b>Linolenic Acid</b><br /><br />&bull; 18:3<br />&bull; PolyUnsaturated<br />&bull; cis, cis,cis-9,12,15-octadecatrienoic acid.<br />(sis, oc-ta-dec-a-tri-e-no-ic)";

function writeHelp(b) {
    var a = document.getElementById("divHelpMsg");
    switch (b) {
    case 0:
        a.innerHTML = "<p>SoapCalc is a calculator for making your own bath soap at home.</p><p>There are many links on this site to help you learn how to make soap, just click <a href='../info/GettingStarted.asp' target='_blank'>Getting Started</a></p><p>SoapCalc is easy to use with <a href='../info/helptext.asp' target='_blank'>SoapCalc Directions</a>.</p><p>Click the blue number buttons for popups containing information about each section.&nbsp; Longer sections are scrollable.</p>";
        break;
    case 1:
        a.innerHTML = "<span style='color:#0000FF;'><strong>Help # " + b + "</strong></span><p>Choose what kind of lye you will be using.  <ul><li>Sodium hydroxide, <strong>NaOH</strong>, is used to make soap bars. &nbsp;If you want to make bar soap, click the circle next to NaOH.</li><br /><li>Potassium hyroxide, <strong>KOH</strong>, is used to make liquid soap. &nbsp;This is a more complicated process than making bar soap.&nbsp; It involves adding the water in two stages: one for the soap cooking stage, the other for diluting the soap.&nbsp; You can find more about it <a href='http://candleandsoap.about.com/od/liquidsoap/ss/basicliquidsoap.htm' target='_blank'>here</a>. &nbsp;When making liquid soap, click the circle next to KOH.&nbsp; If you have 90% pure KOH check the 90% box.</li></ul></p>";
        break;
    case 2:
        a.innerHTML = "<span style='color:#0000FF;'><strong>Help # " + b + "</strong></span><p>This value is the total weight of all the oils that will be used in your recipe. &nbsp;You can use either pounds, ounces or grams as your unit of measurement.</p><p>No matter what unit of measurement you use to create your recipe, the calculated values are displayed in pounds, ounces and grams after clicking the 'View/Print Recipe' button.</p>";
        break;
    case 3:
        a.innerHTML = "<span style='color:#0000FF;'><strong>Help # " +
            b + "</strong></span><p><strong>'Water as % of Oils</strong>' is the simplest way to determine the amount of water used in your recipe. &nbsp;It is recommended that beginners use this method and use the default value of 38%.</p><p>More experienced users typically reduce this percentage to around 32-33%.</p><p>Example: Using the default 38% and the total weight of the oils in your recipe is 1 pound, the amount of water in which the lye is mixed would be 0.38 pounds.  The water and lye amounts are calculated on the 'View Recipe' page.</p><p>'<strong>Lye Concentration</strong>' - Advanced users can set the Lye percentage of the water & lye solution directly.&nbsp; Example:  If you want lye to be 34% of (lye + water), click 'Lye Concentration' and enter 34  in the text box to the right of the check box before clicking 'Calculate Recipe'.<p>'<strong>Water : Lye Ratio</strong>' - Advanced users can also set the Water:Lye ratio directly.&nbsp; Example:  If you want the water to lye ratio to be 2 to 1, click 'Water : Lye Ratio' and enter 2:1 before clicking 'Calculate Recipe'.</p>";
        break;
    case 4:
        a.innerHTML = "<span style='color:#0000FF;'><strong>Help # " + b + "</strong></span><p><strong>Super Fat.</strong>&nbsp; Due to several possible oil variables it is safer to use less lye than needed to saponify the oils in your recipe.&nbsp; This is called a <strong>lye discount</strong> and 5% is generally considered a safe number to use.</p><p>Because discounting the lye by 5% leaves 5% of unsaponified oils in your soap bar this number is also referred to as <strong>super fat</strong>.&nbsp; That extra 5% of superfat not only is a safety factor, it also gives the soap bar extra skin conditioning qualities because approximately 95% of the bar is soap and approximately 5% of the bar is an oil mixture that is deposited on the skin. &nbsp;For more info see <a href='../info/FAQ.asp' target='_blank'>FAQ Number 7</a></p><p><strong>Fragrance.</strong>&nbsp; The amount of fragrance to use is typically about 5% of the total weight of the oils used in your recipe.&nbsp; This is commonly specified as ounces per pound of oils or grams per kilogram of oils.&nbsp;  Some suppliers will specify a usage rate like this: 'Usage Rate: .6 - .8 oz per pound of oils'.</p><p>Example: Assume fragrance at 5% of total oil weight.&nbsp; If you are working in Ounces or Pounds that would be a fragrance ratio of 0.8 ounces per pound (.8oz/16oz = 5%).&nbsp; The metric equivalent would be 50 grams per kilogram.&nbsp; Usually Essential Oils are stronger and usually require a lower percentage than Fragrance Oils.</p>";
        break;
    case 5:
        a.innerHTML = "<span style='color:#0000FF;'><strong>Help # " + b + "</strong></span><p>When one oil is selected from the list (Oils, Fats and Waxes) the values in the 'One' column will change and be specific for the selected oil.&nbsp; The SAP values will change and the name of the oil will be highlighted or displayed under 'Oils, Fats and Waxes'.&nbsp; Go ahead, select an oil to see the change.</p><p>After you have entered the oils for your recipe and 'Calculate Recipe' is clicked the righthand 'All' column displays the values for your recipe.&nbsp; The next Help #6 explains how to add an oil to the recipe list.</p><p>There are 7 Soap Qualities and 8 Fatty Acids listed.<ul><span><strong>To Pop up more...</strong></span><li>PC - mouse over the word.</li><li>Tablet - tap the word.</ul><ul><span><strong>To close the pop up:</strong></span><li>iPad  - tap any text box just to the right of the words.</li><li>Android - Tap the popup to close it OR tap any text box just to the right of the words.</li></ul><p>At the bottom of this column the ratio of saturated to unsaturated fats is displayed after clicking 'Calculate Recipe'.&nbsp; A typical ratio would be around 40:60 although there are many exceptions to this guideline.</p><p>To learn more about fatty acids in general, click on the 'Fatty Acids' link to Wikipedia at the top of this section.</p><p>To go to the Wikipedia page for a specific fatty acid click on the 'i' button to the right of its name.</p>";
        break;
    case 6:
        a.innerHTML = "<span style='color:#0000FF;'><strong>Help # " + b + "</strong></span><p>To add an oil to your recipe:<br /><ul><li><strong>PC</strong> - select it from the list in the middle and <span style='color:#F00'>double click it</span> or click the '<span style='color:#F00;font-weight:bold;font-size:16px'>+</span>' in the 'Soap Recipe' section just to the right of the oil list.</li><li><br /><strong>Tablets</strong> - press the selection box just under 'Oils, Fats and Waxes', make the oil selection, then press the 'Add' button just below <strong>'Recipe Oil List'</strong>' OR press the '<span style='color:#F00;font-weight:bold;font-size:16px'>+</span>' sign by the appropriate oil number.</li></ul>To remove an oil from your recipe:<br /><ul><li><strong>PC or Tablets</strong> - click the '<span style='color:#F00;font-weight:bold;font-size:18px'>-</span>' next to the oil you want to remove OR Enter the recipe number (1-14) in the text box next to the button 'Remove #', then press the Remove button.</li></ul></p>";
        break;
    case 7:
        a.innerHTML = "<span style='color:#0000FF;'><strong>Help # " + b + "</strong></span><p><strong>'Calculate Recipe'</strong> will compute your recipe.&nbsp; SoapCalc will  pop up friendly reminders if there have been any typos or if any information is invalid or missing.</p><p>After clicking <strong>'Calculate Recipe'</strong>, then click <strong>'View/Print Recipe'</strong> to open a new tab in your browser to view or print the recipe details including lye and water amounts.</p><p><strong>Converting weight recipes to percent.</strong><ol><li>Click the round weight button (oz, lb or gm) at the top of the right column in the Recipe section.</li><li>Enter the oils and weights.</li><li>Fill in other necessary values and click 'Calculate Recipe'.&nbsp; Perentages will display in the  % column.</li><li>Click the round '%' button,  select the recipe number, then click 'Save Recipe'.</li><li>You can now set your own total oil weight for this recipe.</li></ol><br /><strong>'Reset Recipe'</strong> will clear SoapCalc and reset SoapCalc to default values.</p><p>Checking the '<strong>Multiple tabs</strong>' box will open a new View Recipe tab each time you click 'View/Print Recipe'.</p><p>Checking the '<strong>Bold</strong>' box will display bold ingredient values in the View Recipe tab.</p><p><strong>Note</strong>: By design, some buttons are grayed out (disabled) until 'Calculate Recipe' has been clicked.</p>";
        break;
    case 8:
        a.innerHTML = "<span style='color:#0000FF;'><strong>Help # " + b + "</strong></span><p>After clicking 'Calculate Recipe', your recipe can be saved using the 'Save Recipe' button.  Up to 8 recipes can be saved as cookies on your device - choose one of the recipe names in the drop down menu before clicking 'Save Recipe'. (Sorry, the recipe names can not be changed.)</p><p>Recipes will be saved using percentages if the round '%' button is selected; or in weights if the round weight button is selected.</p><p><strong>Note</strong>: By design, some buttons are grayed out (disabled) until 'Calculate Recipe' has been clicked.</p>";
        break;
    default:
        a.innerHTML = "<br /><br />No help available."
    }
}

function Oil(oilindex) {
    
	if (document.getElementById("selOil") == null) return false;
	
	
	var b1 = document.getElementById("selOil");
	
	var b_index = b1.options[oilindex].text;
	var b_value = b1.options[oilindex].value; //oil name
	
	
	var b_split = b_value.split(',')
	
	this.id = b_split[0];
    
    this.sap = b_split[1];
    this.iodine = b_split[2];
    this.ins = b_split[3];
    this.lauric = b_split[4];
    this.myristic = b_split[5];
    this.palmitic = b_split[6];
    this.stearic = b_split[7];
    this.ricinoleic = b_split[8];
    this.oleic = b_split[9];
    this.linoleic = b_split[10];
    this.linolenic = b_split[11];
	this.name = b_value;
	
	this.getProperties=function(x,y){
	
	var c;
    c = this.id;
	
	var a = y;
	
    
    31 == c || 65 == c || 138 == c || 40 == c || 5 == c || 140 == c || 142 == c ? 31 === c ? (Conditioning += 98 * a, Creamy += 2 * a, Hard += 2 * a, INS += 77 * a, Iodine += 92 * a, PolyUnsaturated += 100 * a) : 65 === c ? (Hard += 100 * a, Cleansing += 100 * a, Bubbly += 100 * a, Lauric += 2 * a, Myristic += 1 * a, INS += 324 * a, Iodine += 1 * a, Saturated += 90 * a) : (138 == c && (Hard += 7 * a, Creamy += 6 * a, Conditioning += 93 * a, Oleic += 14 * a, Linoleic += 11 * a, Linolenic += 9 * a, INS += 67 * a, Iodine += 105 * a, Saturated += 6 * a, MonoUnsaturated += 70 * a, PolyUnsaturated += 24 *
        a), 40 === c ? (Hard += 5 * a, Creamy += 1 * a, Conditioning += 95 * a, Oleic += 17 * a, Linoleic += 13 * a, Linolenic += 9 * a, INS += 69 * a, Iodine += 106 * a, Saturated += 7 * a, MonoUnsaturated += 68 * a, PolyUnsaturated += 25 * a) : 5 === c ? (Hard += 90 * a, Creamy += 50 * a, Conditioning += 50 * a, Oleic += 0 * a, Linoleic += 0 * a, Linolenic += 0 * a, INS += 84 * a, Iodine += 10 * a) : 140 === c ? (Hard += 28 * a, Creamy += 3 * a, Conditioning += 72 * a, Myristic += 5 * a, Palmitic += 19 * a, Stearic += 2 * a, Oleic += 23 * a, Linoleic += 2 * a, Linolenic += 1 * a, INS += 69 * a, Iodine += 106 * a, Saturated += 28 * a, MonoUnsaturated += 32 * a, PolyUnsaturated +=
        40 * a) : 142 === c && (Hard += 68 * a, Creamy += 60 * a, Conditioning = 60 * a, Oleic += 0 * a, Linoleic += 0 * a, Linolenic += 0 * a, INS += 12 * a, Iodine += 106 * a)) : (c = parseInt(this.lauric), 0 < c && (Hard += c * a, Cleansing += c * a, Bubbly += c * a, Lauric += c * a, Saturated += c * a), c = parseInt(this.myristic), 0 < c && (Hard += c * a, Cleansing += c * a, Bubbly += c * a, Myristic += c * a, Saturated += c * a), c = parseInt(this.palmitic), 0 < c && (Hard += c * a, Creamy += c * a, Palmitic += c * a, Saturated += c * a), c = parseInt(this.stearic), 0 < c && (Hard += c * a, Creamy += c * a, Stearic +=
        c * a, Saturated += c * a), c = parseInt(this.ricinoleic), 0 < c && (Conditioning += c * a, Bubbly += c * a, Creamy += c * a, Ricinoleic += c * a, MonoUnsaturated += c * a), c = parseInt(this.oleic), 0 < c && (Conditioning += c * a, Oleic += c * a, MonoUnsaturated += c * a), c = parseInt(this.linoleic), 0 < c && (Conditioning += c * a, Linoleic += c * a, PolyUnsaturated += c * a), c = parseInt(this.linolenic), 0 < c && (Conditioning += c * a, Linolenic += c * a, PolyUnsaturated += c * a), INS += this.ins * a, Iodine += this.iodine * a)
	}
	}



function twOnClick() {
    !0 === document.getElementById("oilWeight0").checked && (g_radio = 0, unit = "lb", document.getElementById("FragranceRatioUnit").innerHTML = "Oz per Lb", document.getElementById("txtTW").value = "1", document.getElementById("lbloilWeight0").innerHTML = "<strong>Funta</strong>", document.getElementById("lbloilWeight1").innerHTML = "Unci", document.getElementById("lbloilWeight2").innerHTML = "Gram");
    !0 === document.getElementById("oilWeight1").checked && (document.getElementById("TWunit").innerHTML = "unca",
        document.getElementById("lblWT").innerHTML = "oz", document.getElementById("lbloilWeight0").innerHTML = "Funta", document.getElementById("lbloilWeight1").innerHTML = "<strong>Unca</strong>", document.getElementById("lbloilWeight2").innerHTML = "Gram", g_radio = 1, unit = "oz", document.getElementById("FragranceRatioUnit").innerHTML = "Oz per Lb", document.getElementById("txtTW").value = "16");
    !0 === document.getElementById("oilWeight2").checked && (document.getElementById("TWunit").innerHTML = "Grams", document.getElementById("lblWT").innerHTML =
        "gm", document.getElementById("lbloilWeight0").innerHTML = "Funta", document.getElementById("lbloilWeight1").innerHTML = "Unca", document.getElementById("lbloilWeight2").innerHTML = "<strong>Grama</strong>", g_radio = 2, unit = "gm", document.getElementById("FragranceRatioUnit").innerHTML = "Gm per Kg", document.getElementById("txtTW").value = "500");
    document.getElementById("txtTW").focus()
}

function selOilOnChange() {
	resetProps();
    var b1 = document.getElementById("selOil");
	var b_index = b1.selectedIndex
	var b_value = b1.options[b1.selectedIndex].value //oil name
	
	
	var b_split = b_value.split(',')
	
	var Oil1 = new Oil(b_index);
	
    b = Oil1.sap
	Oil1.getProperties(b_index,1);

	document.getElementById("spnHard").value = " " + Hard;
    document.getElementById("spnCleansing").value = " " + Cleansing;
    document.getElementById("spnConditioning").value = " " + Conditioning;
    document.getElementById("spnBubbly").value = " " + Bubbly;
    document.getElementById("spnCreamy").value = " " + Creamy;
    document.getElementById("spnIodine").value = " " + Iodine;
    document.getElementById("spnINS").value =
        " " + INS;
    document.getElementById("spnLauric").value = " " + roundNumber(Oil1.lauric, 0);
	document.getElementById("spnMyristic").value = " " + roundNumber(Oil1.myristic, 0);
	document.getElementById("spnPalmitic").value = " " + roundNumber(Oil1.palmitic, 0);
	document.getElementById("spnStearic").value = " " + roundNumber(Oil1.stearic, 0);
	document.getElementById("spnRicinoleic").value = " " + roundNumber(Oil1.ricinoleic, 0);
	document.getElementById("spnOleic").value = " " + roundNumber(Oil1.oleic, 0);
    document.getElementById("spnLinoleic").value = " " + roundNumber(Oil1.linoleic, 0);
    document.getElementById("spnLinolenic").value = " " + roundNumber(Oil1.linolenic, 0);
    
   
    document.getElementById("txtSAPKOH").value = Oil1.sap;
    document.getElementById("txtSAPNaOH").value = roundNumber(40 / 56.1 * Oil1.sap, 3)
}

function resetAllArraysAndHdnVals() {
    for (var b = 0; 14 > b; b++) document.getElementById("hdnValue" + b).value = "-1", document.getElementById("hdnSap" + b).value = "-1", document.getElementById("hdnIndex" + b).value = "-1", ary14RecipeWeightVals[b] = 0, ary14RecipeSapVals[b] = 0, ary14RecipeSelIndexes[b] = 0, ary14RecipePercentVals[b] = 0, ary14SelectedOilIds[b] = -1
}

function resetPCandWTArrays() {
    for (var b = 0; 14 > b; b++) ary14RecipeWeightVals[b] = 0, ary14RecipePercentVals[b] = 0
}

function clearAll(b) {
    if (!b || window.confirm("This will clear all values in the calculator and set it to its default state.\n\nClick OK or press the Enter key to do this.")) {
        textChanged();
        for (b = 0; 14 > b; b++) removeOil(b);
        resetProps();
        resetAllArraysAndHdnVals();
        document.getElementById("spnHard2").value = " ";
        document.getElementById("spnCleansing2").value = " ";
        document.getElementById("spnConditioning2").value = " ";
        document.getElementById("spnBubbly2").value = " ";
        document.getElementById("spnCreamy2").value = " ";
        document.getElementById("spnIodine2").value = " ";
        document.getElementById("spnINS2").value = " ";
        document.getElementById("spnLauric2").value = " ";
        document.getElementById("spnLinoleic2").value = " ";
        document.getElementById("spnLinolenic2").value = " ";
        document.getElementById("spnMyristic2").value = " ";
        document.getElementById("spnOleic2").value = " ";
        document.getElementById("spnPalmitic2").value = " ";
        document.getElementById("spnRicinoleic2").value = " ";
        document.getElementById("spnStearic2").value = " ";
        document.getElementById("tdSatRatioMain").value =
            " ";
        document.getElementById("hdnHard2").value = "-1";
        document.getElementById("hdnCleansing2").value = "-1";
        document.getElementById("hdnConditioning2").value = "-1";
        document.getElementById("hdnBubbly2").value = "-1";
        document.getElementById("hdnCreamy2").value = "-1";
        document.getElementById("hdnIodine2").value = "-1";
        document.getElementById("hdnINS2").value = "-1";
        document.getElementById("hdnLauric2").value = "-1";
        document.getElementById("hdnMyristic2").value = "-1";
        document.getElementById("hdnPalmitic2").value = "-1";
        document.getElementById("hdnStearic2").value = "-1";
        document.getElementById("hdnRicinoleic2").value = "-1";
        document.getElementById("hdnOleic2").value = "-1";
        document.getElementById("hdnLinoleic2").value = "-1";
        document.getElementById("hdnLinolenic2").value = "-1";
        document.getElementById("hydroxide0").checked = !0;
        document.getElementById("oilWeight0").checked = !0;
        document.getElementById("waterAmount0").checked = !0;
        document.getElementById("txtTW").value = "1";
        setWaterPercent(!1);
        document.getElementById("chkHowToPrint").checked = !1;
        document.getElementById("chkBold").checked = !1;
        document.getElementById("spnLyeConc").value = "";
        document.getElementById("spnWaterLyeRatio").value = "";
        document.getElementById("txtDiscount").value = "5";
        document.getElementById("selOil").selectedIndex = 0;
        document.getElementById("txtFragranceRatio").value = "";
        document.getElementById("txtTotalPerCent").value = "";
        document.getElementById("txtTotalRecipeWeight").value = "";
        document.getElementById("spnFragranceRequired").value = ""
    }
}

function addOil(b) {
    for (var a = document.getElementById("selOil"), c = a.options[a.selectedIndex].text, d = parseInt(a.options[a.selectedIndex].id.substr(2), 10), g = 0; 14 > g; g++)
        if (ary14SelectedOilIds[g] == d) {
            window.alert(c + " is already on the list.");
            return
        }
    ary14SelectedOilIds[b] = d;
    document.getElementById("txtOil" + (b + 1)).value = c;
    document.getElementById("hdnIndex" + b).value = a.selectedIndex;
    document.getElementById("hdnSap" + b).value = a.options[a.selectedIndex].value;
    ary14RecipeSelIndexes[b] = parseInt(a.selectedIndex,
        10);
    ary14RecipeSapVals[b] = parseFloat(a.options[a.selectedIndex].value);
    textChanged()
}

function autoAddOil() {
    for (var b = 0; 14 > b; b++)
        if (0 === document.getElementById("txtOil" + (b + 1)).value.length) {
            addOil(b);
            break
        }
}

function removeOil(b) {
    document.getElementById("hdnIndex" + b).value = "-1";
    document.getElementById("txtOil" + (b + 1)).value = "";
    document.getElementById("hdnSap" + b).value = "-1";
    ary14RecipeSelIndexes[b] = 0;
    ary14RecipeSapVals[b] = 0;
    ary14SelectedOilIds[b] = -1;
    document.getElementById("hdnValue" + b).value = "-1";
    document.getElementById("txtPerCent" + (b + 1)).value = "";
    document.getElementById("txtWeight" + (b + 1)).value = "";
    textChanged()
}

function removeOilTablet() {
    var b;
    b = document.getElementById("txtRemoveItem").value;
    if (!0 == isWhitespace(b)) return alert("No item to remove."), !1;
    b = parseInt(b, 10);
    if (isNaN(b)) return alert("Item to be removed must be a number."), !1;
    if (14 < b || 1 > b) return alert("Item to be removed must be 1 to 14"), !1;
    removeOil(b - 1);
    document.getElementById("txtRemoveItem").value = ""
}

function resetProps() {
    PolyUnsaturated = MonoUnsaturated = Saturated = Stearic = Ricinoleic = Palmitic = Oleic = Myristic = Linolenic = Linoleic = Lauric = INS = Iodine = Creamy = Conditioning = Bubbly = Cleansing = Hard = 0
}

function validate() {
    var b = document.getElementById("txtTW").value,
        a = document.getElementById("radPercent0"),
        c, d, g;
    for (c = 1; 14 >= c; c++) d += document.getElementById("txtOil" + c).value;
    if (0 === d.length) return window.alert("Please add at least one oil or fat to your recipe.\n\nClick help button 6 for instructions on how to add or remove an oil."), !1;
    g = 0;
    if (!0 === a.checked) {
        if (0 === b.length) return window.alert("Please fill in Total Oil Weight."), !1;
        if (isNaN(b)) return window.alert("Total Oil Weight is not a number."), !1;
        c = parseFloat(b);
        if (0 >= c) return window.alert("Total Oil Weight can not be less than 0 (zero)."), !1;
        for (c = 1; 14 >= c; c++)
            if (d = document.getElementById("txtOil" + c).value, b = document.getElementById("txtPerCent" + c).value, !isEmpty(d)) {
                if (isEmpty(b)) return window.alert("Please enter a value for Oil Number " + c + ", " + d + ", or remove it from the recipe."), !1;
                d = parseFloat(b);
                if (isNaN(d)) return window.alert("Value for Oil Number " + c + " is not a number"), !1;
                if (0 >= parseFloat(b)) return window.alert("Percent value for Oil Number " +
                    c + " can not be zero or less. Either remove oil or put in a number greater than zero."), !1;
                g += d
            } else if (!isEmpty(b)) return window.alert("There is no oil name listed for Oil Number " + c + "  but the percentage column contains a value.\n\nPlease either add an oil for number " + c + " or click the minus sign next to item " + c + " to remove the percentage."), !1;
        c = roundNumber(g, 1);
        document.getElementById("txtTotalPerCent").value = c;
        if (100 != c) return c = roundNumber(100 - c, 4), document.getElementById("txtTotalPerCent").style.color =
            "#FF0000", window.alert("Total Per Cent does not equal 100%\n\nAdjust by" + (0 < c ? " adding " : " taking away ") + Math.abs(c) + " %"), !1;
        document.getElementById("txtTotalPerCent").style.color = "#000000"
    } else
        for (g = 0, c = 1; 14 >= c; c++)
            if (b = document.getElementById("txtWeight" + c).value, d = document.getElementById("txtOil" + c).value, !isEmpty(d)) {
                if (isEmpty(b)) return window.alert("There is no weight value for Oil Number " + c + ".  \n\nEither add a weight value for Oil Number " + c + " or click the minus sign next to item " + c +
                    " to remove it."), !1;
                if (isNaN(b) && 0 < b.length) return window.alert("Weight value for Oil Number " + c + " is not a number"), !1;
                if (0 >= parseFloat(b)) return window.alert("Weight value for Oil Number " + c + " can not be zero or less. Either remove oil or put in a number greater than zero."), !1;
                g += parseFloat(b)
            } else if (!isEmpty(b)) return window.alert("There is no oil name listed for Oil Number " + c + "  but the weight column contains a value.\n\nPlease either add an oil for number " + c + " or click the minus sign next to item " +
        c + " to remove the weight."), !1;
    if (!1 === document.getElementById("waterAmount1").checked && !1 === document.getElementById("waterAmount2").checked && (c = document.getElementById("txtWaterPercent").value, isWhitespace(c) || isNaN(c))) return window.alert('"Water as % of Oils" is not a number.'), !1;
    c = document.getElementById("txtDiscount").value;
    if (isWhitespace(c) || isNaN(c)) document.getElementById("txtDiscount").value = "0";
    c = document.getElementById("txtFragranceRatio").value;
    if (0 !== c.length) {
        if (isNaN(c)) return window.alert("Fragrance ratio is not a number"), !1;
        parseFloat(c)
    }
    if (!0 === FixedConc && (c = document.getElementById("spnLyeConc").value, isNaN(parseFloat(c)))) return window.alert("Lye Concentration value must be a number.\nEither uncheck the Lye Concentration 'Set' box or enter a number."), !1;
    if (!0 === FixedRatio) {
        c = document.getElementById("spnWaterLyeRatio").value;
        if (-1 == c.indexOf(":")) return window.alert("The ratio must be expressed as 'Water:Lye'.\nA colon, : , must be be between the water value and the lye value."), !1;
        c = c.split(":");
        if (isNaN(c[0]) || isNaN(c[1])) return window.alert("Water and Lye values in the ratio must both be numbers."), !1
    }
    return !0
}

function ComputeUsingPC() {
    var b, a, c, d, g, f;
    c = g = 0;
    d = parseFloat(document.getElementById("txtTW").value);
    for (var e = 1; 14 >= e; e++) b = parseFloat(document.getElementById("txtPerCent" + e).value), isNaN(b) && (b = 0), ary14RecipePercentVals[e - 1] = b / 100, g += b, a = b * d / 100, 0 < b && (document.getElementById("txtWeight" + e).value = fixPCrounding(a)), ary14RecipeWeightVals[e - 1] = a, c += a, b = document.getElementById("txtOil" + e), f = document.getElementById("txtWeight" + e), f.value = "" === b.value ? "" : a;
    document.getElementById("txtTotalPerCent").value =
        roundNumber(g, 4);
    document.getElementById("txtTotalRecipeWeight").value = roundNumber(c, 3)
}

function ComputeUsingWT() {
    for (var b, a = 0, c = 0, d = 1; 14 >= d; d++) b = document.getElementById("txtWeight" + d).value, 0 < b.length ? (ary14RecipeWeightVals[d - 1] = parseFloat(b), c += ary14RecipeWeightVals[d - 1]) : ary14RecipeWeightVals[d - 1] = 0;
    for (d = 1; 14 >= d; d++) ary14RecipePercentVals[d - 1] = ary14RecipeWeightVals[d - 1] / c * 100, b = document.getElementById("txtPerCent" + d), 0 === ary14RecipePercentVals[d - 1] ? b.value = "" : (b.value = fixPCrounding(ary14RecipePercentVals[d - 1]), a += ary14RecipePercentVals[d - 1]);
    document.getElementById("txtTotalPerCent").value =
        roundNumber(a, 4);
    document.getElementById("txtTotalRecipeWeight").value = roundNumber(c, 3);
    document.getElementById("txtTW").value = roundNumber(c, 3)
}

function computeRecipe(b) {
    var a, c;
    resetPCandWTArrays();
    resetProps();
	
    if (validate()) {
		
        !0 === document.getElementById("radPercent0").checked ? ComputeUsingPC() : ComputeUsingWT();
		
        for (b = g_tot = 0; b < ary14RecipeWeightVals.length; b++) 0 !== ary14RecipeWeightVals[b] && (g_tot += ary14RecipeWeightVals[b]); 
		
        for (b = 1; 14 >= b; b++) {
			if (document.getElementById("txtOil" + b) == null) alert ('g_tot = ' + g_tot);
				a = document.getElementById("txtOil" + b).value;
				
				while (0 < a.length) {
					a = ary14RecipeSelIndexes[b - 1];
					c = ary14RecipeWeightVals[b - 1] / g_tot;
					
					var Oil2 = new Oil(a);
					Oil2.getProperties(a,c);
					}
		}
		document.getElementById("spnHard2").value = " " + roundNumber(Hard,
            0);
        document.getElementById("spnCleansing2").value = " " + roundNumber(Cleansing, 0);
        document.getElementById("spnConditioning2").value = " " + roundNumber(Conditioning, 0);
        document.getElementById("spnBubbly2").value = " " + roundNumber(Bubbly, 0);
        document.getElementById("spnCreamy2").value = " " + roundNumber(Creamy, 0);
        document.getElementById("spnIodine2").value = " " + roundNumber(Iodine, 0);
        document.getElementById("spnINS2").value = " " + roundNumber(INS, 0);
        document.getElementById("spnLauric2").value = " " + roundNumber(Lauric,
            0);
        document.getElementById("spnLinoleic2").value = " " + roundNumber(Linoleic, 0);
        document.getElementById("spnLinolenic2").value = " " + roundNumber(Linolenic, 0);
        document.getElementById("spnMyristic2").value = " " + roundNumber(Myristic, 0);
        document.getElementById("spnOleic2").value = " " + roundNumber(Oleic, 0);
        document.getElementById("spnPalmitic2").value = " " + roundNumber(Palmitic, 0);
        document.getElementById("spnRicinoleic2").value = " " + roundNumber(Ricinoleic, 0);
        document.getElementById("spnStearic2").value = " " + roundNumber(Stearic,
            0);
        b = 0 === document.getElementById("txtFragranceRatio").value.length ? 0 : parseFloat(document.getElementById("txtFragranceRatio").value);
        !0 === document.getElementById("oilWeight0").checked && (document.getElementById("spnFragranceRequired").value = " " + roundNumber(b * g_tot, 3) + " Oz");
        !0 === document.getElementById("oilWeight1").checked && (a = g_tot / 16, document.getElementById("spnFragranceRequired").value = " " + roundNumber(b * a, 3) + " Oz");
        !0 === document.getElementById("oilWeight2").checked && (document.getElementById("spnFragranceRequired").value =
            " " + roundNumber(b * g_tot / 1E3, 2) + " Gm");
        showSatRatio(roundNumber(Saturated, 0), roundNumber(MonoUnsaturated, 0), roundNumber(PolyUnsaturated, 0));
        g_Computed = !0;
        setButtons(!0)
		
    }
}

function textChanged() {
    g_Computed = !1;
    setButtons(!1)
}

function printRecipe() {
    var b, a, c;
    for (a = 1; 14 >= a; a++) b = document.getElementById("txtOil" + a).value, 0 < b.length && (c = ary14RecipeSelIndexes[a - 1], b = ary14RecipeWeightVals[a - 1] / g_tot, getProperties(c, b), !0 === document.getElementById("radPercent0").checked ? document.getElementById("hdnValue" + (a - 1)).value = b : document.getElementById("hdnValue" + (a - 1)).value = ary14RecipeWeightVals[a - 1]);
    document.getElementById("hdnHard2").value = document.getElementById("spnHard2").value;
    document.getElementById("hdnCleansing2").value = document.getElementById("spnCleansing2").value;
    document.getElementById("hdnConditioning2").value = document.getElementById("spnConditioning2").value;
    document.getElementById("hdnBubbly2").value = document.getElementById("spnBubbly2").value;
    document.getElementById("hdnCreamy2").value = document.getElementById("spnCreamy2").value;
    document.getElementById("hdnIodine2").value = document.getElementById("spnIodine2").value;
    document.getElementById("hdnINS2").value = document.getElementById("spnINS2").value;
    document.getElementById("hdnLauric2").value = document.getElementById("spnLauric2").value;
    document.getElementById("hdnMyristic2").value = document.getElementById("spnMyristic2").value;
    document.getElementById("hdnPalmitic2").value = document.getElementById("spnPalmitic2").value;
    document.getElementById("hdnStearic2").value = document.getElementById("spnStearic2").value;
    document.getElementById("hdnRicinoleic2").value = document.getElementById("spnRicinoleic2").value;
    document.getElementById("hdnOleic2").value = document.getElementById("spnOleic2").value;
    document.getElementById("hdnLinoleic2").value =
        document.getElementById("spnLinoleic2").value;
    document.getElementById("hdnLinolenic2").value = document.getElementById("spnLinolenic2").value;
    !0 === g_Computed && (document.getElementById("chkHowToPrint").checked ? document.getElementById("recipeData").target = "_blank" : document.getElementById("recipeData").target = "printWin", document.forms.recipeData.submit())
}

function setButtons(b) {
    b ? (document.getElementById("btnViewRecipe").disabled = !1, document.getElementById("btnSaveRecipe").disabled = !1, document.getElementById("btnViewRecipe").style.cursor = "pointer", document.getElementById("btnSaveRecipe").style.cursor = "pointer") : (document.getElementById("btnViewRecipe").disabled = !0, document.getElementById("btnSaveRecipe").disabled = !0, document.getElementById("btnViewRecipe").style.cursor = "default", document.getElementById("btnSaveRecipe").style.cursor = "default")
}

function setWaterPercent(b) {
    var a = document.getElementById("txtWaterPercent"),
        c = document.getElementById("spnLyeConc"),
        d = document.getElementById("spnWaterLyeRatio");
    FixedRatio = FixedConc = !1;
    a.value = "38";
    a.style.backgroundColor = "#C9FCD0";
    a.readOnly = !1;
    document.getElementById("lblwaterAmount0").style.fontWeight = "900";
    c.readOnly = !0;
    c.value = "0";
    c.style.backgroundColor = "#FFFFFF";
    document.getElementById("lblwaterAmount1").style.fontWeight = "400";
    d.readOnly = !0;
    d.style.backgroundColor = "#FFFFFF";
    d.value = "0";
    document.getElementById("lblwaterAmount2").style.fontWeight =
        "400";
    b && a.focus()
}

function setLyeConcentration(b) {
    var a = document.getElementById("txtWaterPercent"),
        c = document.getElementById("spnLyeConc"),
        d = document.getElementById("spnWaterLyeRatio");
    FixedConc = !0;
    FixedRatio = !1;
    c.style.backgroundColor = "#C9FCD0";
    c.readOnly = !1;
    document.getElementById("lblwaterAmount1").style.fontWeight = "900";
    a.style.backgroundColor = "#FFFFFF";
    a.readOnly = !0;
    a.value = "0";
    document.getElementById("lblwaterAmount0").style.fontWeight = "400";
    d.style.backgroundColor = "#FFFFFF";
    d.readOnly = !0;
    d.value = "0";
    document.getElementById("lblwaterAmount2").style.fontWeight =
        "400";
    b && c.focus()
}

function setWaterLyeRatio(b) {
    var a = document.getElementById("txtWaterPercent"),
        c = document.getElementById("spnLyeConc"),
        d = document.getElementById("spnWaterLyeRatio");
    FixedConc = !1;
    FixedRatio = !0;
    d.style.backgroundColor = "#C9FCD0";
    d.readOnly = !1;
    document.getElementById("lblwaterAmount2").style.fontWeight = "900";
    a.style.backgroundColor = "#FFFFFF";
    a.readOnly = !0;
    a.value = "0";
    document.getElementById("lblwaterAmount0").style.fontWeight = "400";
    c.style.backgroundColor = "#FFFFFF";
    c.readOnly = !0;
    c.value = "0";
    document.getElementById("lblwaterAmount1").style.fontWeight =
        "400";
    b && d.focus()
}

function setFixedWaterMethod(b) {
    if (null == b) return -1;
    g_FixedRatio = g_FixedConc = !1;
    switch (parseInt(b.value, 10)) {
    case 0:
        setWaterPercent(!0);
        break;
    case 1:
        setLyeConcentration(!0);
        break;
    case 2:
        setWaterLyeRatio(!0)
    }
}

function PorWClick() {
    var b;
    g_Computed = !1;
    setButtons(!1);
    if (!0 === document.getElementById("radPercent1").checked) {
        document.getElementById("hdnPorW").value = "1";
        for (b = 1; 14 >= b; b++) document.getElementById("txtPerCent" + b).style.backgroundColor = "#FFFFFF", document.getElementById("txtPerCent" + b).readOnly = !0, document.getElementById("txtWeight" + b).readOnly = !1, document.getElementById("txtWeight" + b).style.backgroundColor = "#C9FCD0";
        document.getElementById("radPercent0").checked = !1;
        document.getElementById("radPercent1").checked = !0;
        document.getElementById("txtTW").style.backgroundColor = "#FFFFFF";
        document.getElementById("txtTW").readOnly = !0
    } else {
        document.getElementById("hdnPorW").value = "0";
        for (b = 1; 14 >= b; b++) document.getElementById("txtPerCent" + b).style.backgroundColor = "#C9FCD0", document.getElementById("txtPerCent" + b).readOnly = !1, document.getElementById("txtWeight" + b).style.backgroundColor = "#FFFFFF", document.getElementById("txtWeight" + b).readOnly = !0, document.getElementById("txtWeight" + b).style.backgroundColor = "#FFFFFF";
        document.getElementById("radPercent0").checked = !0;
        document.getElementById("radPercent1").checked = !1;
        document.getElementById("txtTW").style.backgroundColor = "#C9FCD0";
        document.getElementById("txtTW").readOnly = !1
    }
}

function hydroxideChange(b) {
    null == b && (b = document.getElementsByName("R2")[0].checked ? document.getElementById("hydroxide0") : document.getElementById("hydroxide1"));
    1 == b.value ? (document.getElementById("lblNaOH").innerHTML = "&nbsp;NaOH", document.getElementById("lblKOH").innerHTML = "<strong>&nbsp;KOH</strong>", document.getElementById("chkNintyPerCent").disabled = !1) : (document.getElementById("lblNaOH").innerHTML = "<strong>&nbsp;NaOH</strong>", document.getElementById("lblKOH").innerHTML = "&nbsp;KOH", document.getElementById("chkNintyPerCent").disabled = !0, document.getElementById("chkNintyPerCent").checked = !1);
    g_Computed = !1
}

function kohPurityChanged() {
    g_90PercentKOH = !0 === document.getElementById("chkNintyPerCent").checked ? !0 : !1;
    textChanged()
}

function SaveRecipe() {
    var b, a, c, d, g, f, e = c = "";
    b = new Date;
    a = b.getTime() + 31536E6;
    b.setTime(a);
    g = document.getElementById("selRecipeName").value;
    for (a = 1; 14 >= a; a++) d = !0 === document.getElementById("radPercent0").checked ? document.getElementById("txtPerCent" + a) : document.getElementById("txtWeight" + a), f = document.getElementById("hdnIndex" + (a - 1)), c = -1 != f.value ? 14 == a ? c + (aryAllOils[f.value].id + "^" + d.value + "#") : c + (aryAllOils[f.value].id + "^" + d.value + "@") : 14 == a ? c + (f.value + "^0#") : c + (f.value + "^0@");
    !0 === document.getElementById("oilWeight0").checked &&
        (e += "unit^lb@");
    !0 === document.getElementById("oilWeight1").checked && (e += "unit^oz@");
    !0 === document.getElementById("oilWeight2").checked && (e += "unit^gm@");
    e += "wt^" + document.getElementById("txtTW").value + "@";
    e += "wpc^" + document.getElementById("txtWaterPercent").value + "@";
    e += "wdis^" + document.getElementById("txtDiscount").value + "@";
    e += "lyecon^" + document.getElementById("spnLyeConc").value + "@";
    e += "wlratio^" + document.getElementById("spnWaterLyeRatio").value + "@";
    e += "chklc^" + document.getElementById("waterAmount1").checked +
        "@";
    e += "chkwl^" + document.getElementById("waterAmount2").checked + "@";
    e += "pw^" + document.getElementById("radPercent0").checked + "@";
    e += "wpc^" + document.getElementById("txtWaterPercent").value + "@";
    e += "wdis^" + document.getElementById("txtDiscount").value + "@";
    e += "naoh^" + document.getElementById("hydroxide0").checked + "@";
    e += "frag^" + document.getElementById("txtFragranceRatio").value + "@";
    e = document.getElementById("chkNintyPerCent").checked ? e + "ninty^true" : e + "ninty^false";
    document.cookie = g + "=" + (c + e) + ";expires=" +
        b.toGMTString();
    b = document.getElementById("divRecipeSaved");
    b.style.display = "block";
    fade(b)
}

function fade(b) {
    var a = 1,
        c = setInterval(function () {
            0.1 >= a && (clearInterval(c), b.style.display = "none");
            b.style.opacity = a;
            b.style.filter = "alpha(opacity=" + 100 * a + ")";
            a -= 0.05 * a
        }, 50)
}

function GetCookie(b) {
    var a = document.cookie,
        c = a.indexOf(b);
    return -1 != c ? (b = c + b.length + 1, c = a.indexOf(";", b), -1 == c && (c = a.length), a = a.substring(b, c), unescape(a)) : ""
}

function GetRecipe() {
    var b = document.getElementById("selRecipeName").value,
        a = [],
        c = [],
        d, g, f = GetCookie(b),
        e = 0,
        l = document.getElementById("selOil");
    g = b.lastIndexOf("e");
    d = b.substring(0, g + 1);
    g = b.substring(g + 1);
    var b = 0,
        h;
    clearAll(!1);
    d += " " + g;
    if (0 === f.length) window.alert(d + " has not been saved as a cookie on your computer.");
    else {
        for (d = 1; 14 >= d; d++) document.getElementById("txtWeight" + d).value = "", document.getElementById("txtPerCent" + d).value = "";
        document.getElementById("spnFragranceRequired").value = "";
        document.getElementById("txtTotalPerCent").value =
            "";
        document.getElementById("txtTotalRecipeWeight").value = "";
        d = document.getElementById("txtWaterPercent");
        g = document.getElementById("spnLyeConc");
        h = document.getElementById("spnWaterLyeRatio");
        for (var n = document.getElementById("lblwaterAmount0"), p = document.getElementById("lblwaterAmount1"), q = document.getElementById("lblwaterAmount2"), r = f.split("#"), m = r[0].split("@"), k, f = 0; f < m.length; f++) k = m[f].split("^"), a[f] = k[0], c[f] = k[1];
        m = r[1].split("@");
        for (f = 0; f < m.length; f++) switch (k = m[f].split("^"), k[0]) {
        case "unit":
            "lb" ==
                k[1] && (document.getElementById("oilWeight0").checked = !0, document.getElementById("lblWT").innerHTML = "lb", document.getElementById("TWunit").innerHTML = "Pounds", document.getElementById("FragranceRatioUnit").innerHTML = "Oz per Lb", unit = "lb");
            "oz" == k[1] && (document.getElementById("oilWeight1").checked = !0, document.getElementById("lblWT").innerHTML = "oz", document.getElementById("TWunit").innerHTML = "Ounces", document.getElementById("FragranceRatioUnit").innerHTML = "Oz per Lb", unit = "oz");
            "gm" == k[1] && (document.getElementById("oilWeight2").checked = !0, document.getElementById("lblWT").innerHTML = "gm", document.getElementById("TWunit").innerHTML = "Grams", document.getElementById("FragranceRatioUnit").innerHTML = "Gm per Kg", unit = "gm");
            break;
        case "wt":
            document.getElementById("txtTW").value = k[1];
            document.getElementById("txtTotalRecipeWeight").value = k[1];
            break;
        case "wpc":
            document.getElementById("txtWaterPercent").value = k[1];
            break;
        case "wdis":
            document.getElementById("txtDiscount").value = k[1];
            break;
        case "lyecon":
            document.getElementById("spnLyeConc").value =
                k[1];
            break;
        case "wlratio":
            document.getElementById("spnWaterLyeRatio").value = k[1];
            break;
        case "chklc":
            "true" == k[1] ? (document.forms.recipeData.elements.R4[1].checked = !0, g.style.backgroundColor = "#C9FCD0", g.focus(), g.readOnly = !1, p.style.fontWeight = "900", d.value = "", h.value = "", FixedConc = !0, e = 1) : (document.forms.recipeData.elements.R4[1].checked = !1, g.style.backgroundColor = "#FFFFFF", g.readOnly = !0, p.style.fontWeight = "400", FixedConc = !1);
            break;
        case "chkwl":
            "true" == k[1] ? (document.forms.recipeData.elements.R4[2].checked = !0, h.style.backgroundColor = "#C9FCD0", h.focus(), h.readOnly = !1, q.style.fontWeight = "900", d.value = "", g.value = "", FixedRatio = !0, e = 2) : (document.forms.recipeData.elements.R4[2].checked = !1, h.style.backgroundColor = "#FFFFFF", h.readOnly = !0, q.style.fontWeight = "400", FixedRatio = !1);
            break;
        case "pw":
            "true" == k[1] ? (document.getElementById("radPercent0").checked = !0, document.getElementById("txtTW").style.backgroundColor = "#C9FCD0") : (document.getElementById("radPercent1").checked = !0, document.getElementById("txtTW").style.backgroundColor =
                "#FFFFFF");
            document.getElementById("txtTW").readOnly = !0;
            break;
        case "wpc":
            document.getElementById("txtWaterPercent").value = k[1];
            break;
        case "wdis":
            document.getElementById("txtDiscount").value = k[1];
            break;
        case "naoh":
            hydroxideChange(document.getElementById("hydroxide0"));
            "true" === k[1] ? document.getElementById("hydroxide0").checked = !0 : (document.getElementById("hydroxide1").checked = !0, document.getElementById("chkNintyPerCent").disabled = !1);
            break;
        case "frag":
            document.getElementById("txtFragranceRatio").value =
                k[1];
            break;
        case "ninty":
            "true" === k[1] ? document.getElementById("chkNintyPerCent").checked = !0 : document.getElementById("chkNintyPerCent").checked = !1
        }!1 === document.forms.recipeData.elements.R4[1].checked && !1 === document.forms.recipeData.elements.R4[2].checked ? (document.forms.recipeData.elements.R4[0].checked = !0, d.style.backgroundColor = "#C9FCD0", d.readOnly = !1, n.style.fontWeight = "900", g.value = "", h.value = "", FixedConc = !1) : (d.style.backgroundColor = "#FFFFFF", d.readOnly = !0, n.style.fontWeight = "400");
        for (f = 0; f <
            a.length; f++)
            if (d = f + 1, h = !0 === document.getElementById("radPercent0").checked ? document.getElementById("txtPerCent" + d) : document.getElementById("txtWeight" + d), d = document.getElementById("txtOil" + d), g = document.getElementById("hdnIndex" + f), -1 == a[f]) d.value = "", h.value = "", g.value = "-1", ary14RecipeSapVals[f] = 0, ary14RecipeSelIndexes[f] = 0;
            else {
                isNaN(c[f]) ? h.value = "" : (h.value = c[f], !0 === document.getElementById("radPercent0").checked && (b += parseFloat(c[f])));
                for (h = 0; h < aryAllOils.length && a[f] != aryAllOils[h].id; h++);
                d.value = l.options[h].text;
                ary14RecipeSelIndexes[f] = h;
                g.value = h;
                ary14RecipeSapVals[f] = parseFloat(l.options[h].value);
                document.getElementById("hdnSap" + f).value = l.options[h].value
            }!0 === document.getElementById("radPercent0").checked && (document.getElementById("txtTotalPerCent").value = b, 100 != roundNumber(b, 1) ? document.getElementById("txtTotalPerCent").style.color = "#FF0000" : document.getElementById("txtTotalPerCent").style.color = "#000000");
        PorWClick();
        setFixedWaterMethod(e)
    }
}

function roundNumber(b, a) {
    return Math.round(b * Math.pow(10, a)) / Math.pow(10, a)
}

function isWhitespace(b) {
    var a;
    if (isEmpty(b)) return !0;
    for (a = 0; a < b.length; a++) {
        var c = b.charAt(a);
        if (-1 == " \t\n\r".indexOf(c)) return !1
    }
    return !0
}

function isEmpty(b) {
    return null === b || 0 === b.length
}

function fixPCrounding(b) {
    var a;
    a = b.toString();
    if (-1 == a.indexOf(".")) return b;
    a = a.split(".");
    b = a[0];
    a = a[1].slice(0, 12);
    return parseFloat(b + "." + a)
}

function hideHelp() {
    document.getElementById("divOnPageHelp").style.display = "none"
}

function showHelp(b) {
    b = parseInt((b.id + "").substr(4), 10);
    var a = document.getElementById("divOnPageHelp");
    a.style.height = 0 == b ? "230px" : 2 == b ? "220px" : "250px";
    switch (b) {
    case 0:
        a.style.top = "250px";
        a.style.left = "165px";
        writeHelp(0);
        break;
    case 1:
        a.style.top = "370px";
        a.style.left = "165px";
        writeHelp(1);
        break;
    case 2:
        a.style.top = "370px";
        a.style.left = "165px";
        writeHelp(2);
        break;
    case 3:
        a.style.top = "370px";
        a.style.left = "390px";
        writeHelp(3);
        break;
    case 4:
        a.style.top = "370px";
        a.style.left = "540px";
        writeHelp(4);
        break;
    case 5:
        a.style.top =
            "60px";
        a.style.left = "165px";
        writeHelp(5);
        break;
    case 6:
        a.style.top = "90px";
        a.style.left = "365px";
        writeHelp(6);
        break;
    case 7:
        a.style.top = "525px";
        a.style.left = "365px";
        writeHelp(7);
        break;
    case 8:
        a.style.top = "90px", a.style.left = "455px", writeHelp(8)
    }
    a.style.display = "block";
    document.getElementById("closeHelp1").style.height = document.getElementById("divHelpMsg").clientHeight + "px";
    0 < b && (document.getElementById("divOnPageHelp").scrollTop =
        0)
}

function closeOnPageHelp() {
    document.getElementById("divOnPageHelp").style.display = "none"
}

function showCount() {
    document.getElementById("spnCount").style.display = "inline"
}

function hideCount() {
    document.getElementById("spnCount").style.display = "none"
}

function showFADetail(b) {
    document.getElementById("divFADetail").innerHTML = aryFADetail[b] + tailFADetail;
    document.getElementById("divFADetail").style.display = "block"
}

function hideFADetail() {
    document.getElementById("divFADetail").style.display = "none"
}

function showRanges(b) {
    document.getElementById("divRanges").innerHTML = aryRanges[b];
    document.getElementById("divRanges").style.display = "block"
}

function hideRanges() {
    document.getElementById("divRanges").style.display = "none"
}

function showSatRatio(b, a, c) {
    a += c;
    var d = 100 - (b + a);
    c = roundNumber(b / (b + a) * d, 0);
    d = roundNumber(a / (b + a) * d, 0);
    b = b + c + " : " + (a + d);
    document.getElementById("tdSatRatioMain").value = b
};