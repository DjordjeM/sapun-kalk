<?php


function sapunkalk_settings_menu() {
	add_options_page( 'Sapun kalkulator',
	'Sapun kalkulator', 'manage_options',
	'sapun-kalk',
	'sapunkalk_config_page' );
}

add_action( 'admin_menu', 'sapunkalk_settings_menu' );

add_action( 'admin_init', 'sapunkalk_admin_init' );



function process_sapunkalk_sirovina() {
	
	// Check if user has proper security level
	if ( !current_user_can( 'manage_options' ) )
	wp_die( 'Not allowed' );

	// Check if nonce field is present for security
	check_admin_referer( 'sapunkalk_add_edit' );
	
	global $wpdb;
	
	
	// Place all user submitted values in an array (or empty
	// strings if no value was sent)
	$sirovine_data = array();
	
	$sirovine_data['sirovina_naziv'] = ( isset( $_POST['sirovina_naziv'] ) ? $_POST['sirovina_naziv'] : '' );
	$sirovine_data['sap'] = ( isset( $_POST['sap'] ) ? 	$_POST['sap'] : 0 );
	$sirovine_data['iodine'] = ( isset( $_POST['iodine'] ) ? $_POST['iodine'] : 0);
	$sirovine_data['ins'] = ( isset( $_POST['ins'] ) ? $_POST['ins'] : 0 );
	$sirovine_data['lauric'] = ( isset( $_POST['lauric'] ) ? $_POST['lauric'] : 0 );
	$sirovine_data['myristic'] = ( isset( $_POST['myristic'] ) ? $_POST['myristic'] : 0 );
	$sirovine_data['palmitic'] = ( isset( $_POST['palmitic'] ) ? $_POST['palmitic'] : 0 );
	$sirovine_data['stearic'] = ( isset( $_POST['stearic'] ) ? $_POST['stearic'] : 0 );
	$sirovine_data['ricinoleic'] = ( isset( $_POST['ricinoleic'] ) ? $_POST['ricinoleic'] : 0 );
	$sirovine_data['oleic'] = ( isset( $_POST['oleic'] ) ? $_POST['oleic'] : 0 );
	$sirovine_data['linoleic'] = ( isset( $_POST['linoleic'] ) ? $_POST['linoleic'] : 0 );
	$sirovine_data['linolenic'] = ( isset( $_POST['linolenic'] ) ? $_POST['linolenic'] : 0 ); 
	
	
	// Call the wpdb insert or update method based on value
	// of hidden bug_id field
	if ( isset( $_POST['sirovina_id'] ) && $_POST['sirovina_id'] == 'new') {
		$wpdb->insert( $wpdb->get_blog_prefix() . 'sapunkalk', $sirovine_data );
	} elseif ( isset( $_POST['sirovina_id']) && is_numeric( $_POST['sirovina_id'] ) ) {
		$wpdb->update( $wpdb->get_blog_prefix() . 'sapunkalk', $sirovine_data, array( 'sirovina_id' => $_POST['sirovina_id'] ) ) ;
		}
	
	// Redirect the page to the user submission form
	
		wp_redirect( add_query_arg('page','sapun-kalk',admin_url('options-general.php')));
	exit;
}

function sapunkalk_admin_init() {
	add_action( 'admin_post_save_sapunkalk', 'process_sapunkalk_sirovina' );
	add_action( 'admin_post_delete_sapunkalk', 'delete_sapunkalk_sirovina' );
}

function delete_sapunkalk_sirovina() {
		// Check that user has proper security level
		if ( !current_user_can( 'manage_options' ) )
			wp_die( 'Not allowed' );
		// Check if nonce field is present
			check_admin_referer( 'sapunkalk_deletion' );
		// If bugs are present, cycle through array and call SQL
		// command to delete entries one by one
		if ( !empty( $_POST['sastojci'] ) ) {
		// Retrieve array of bugs IDs to be deleted
			$sirovine_delete = $_POST['sastojci'];
			global $wpdb;
			foreach ( $sirovine_delete as $sirovina_delete ) {
				$query = 'DELETE from ' . $wpdb->get_blog_prefix();
				$query .= 'sapunkalk ';
				$query .= 'WHERE sirovina_id = ' . intval( $sirovina_delete );
				var_dump ($sirovine_delete);
				$wpdb->query( $wpdb->prepare( $query ) );
			}
		}
		// Redirect the page to the user submission form
		wp_redirect( add_query_arg( 'page', 'sapun-kalk', admin_url( 'options-general.php' ) ) );
		exit;
	}

function sapunkalk_config_page() {
	global $wpdb;
	?>
	<!-- Top-level menu -->
	<div id="sapun-kalk-general" class="wrap">
		<h2>Kalkulator - Sirovine <a class="add-new-h2" href="<?php echo
			add_query_arg( array( 'page' => 'sapun-kalk',
									'id' => 'new' ),
			admin_url('options-general.php') ); ?>">
			Dodaj novu sirovinu</a>
		</h2>
	<!-- Prika�i listu sirovina ako parametar nije poslat preko URL-a -->
	<?php if ( empty( $_GET['id'] ) ) {
			$sapunkalk_query = 'select * from ';
			$sapunkalk_query .= $wpdb->get_blog_prefix() . 'sapunkalk ';
			$sapunkalk_query .= 'ORDER by 2 ASC';
			
			$sirovine =
			$wpdb->get_results( $wpdb->prepare( $sapunkalk_query,10 ), ARRAY_A);
			
			?>
			<h3>Lista sirovina</h3>
			
			<form method="post" action="<?php echo admin_url( 'admin-post.php' ); ?>">
				<input type="hidden" name="action" value="delete_sapunkalk" />
				<!-- Adding security through hidden referrer field -->
				<?php wp_nonce_field( 'sapunkalk_deletion' ); ?>
			
			<table class="wp-list-table widefat fixed" >
				<thead>
					<tr>
						<th style="width: 20px">&nbsp;</th>
						<th style="width: 30px">ID</th>
						<th style="width: 120px">Naziv</th>
						<th style="width: 40px; align=center">SAP</th>
						<th style="width: 40px; align=center">Iodine</th>
						<th style="width: 40px; align=center">INS</th>
						<th style="width: 40px; align=center; align=center">Lauric</th>
						<th style="width: 40px; align=center">Myristic</th>
						<th style="width: 40px; align=center">Palmitic</th>
						<th style="width: 40px; align=center">Stearic</th>
						<th style="width: 40px; align=center">Ricinoleic</th>
						<th style="width: 40px; align=center">Oleic</th>
						<th style="width: 40px; align=center">Linoleic</th>
						<th style="width: 40px; align=center">Linolenic</th>
					</tr>
				</thead>
			<?php
			// Prika�i sirovine ako upit vrati rezultate
			if ( $sirovine ) {
				foreach ( $sirovine as $sirovina ) {
					echo '<tr style="background: #FFF">';
					echo '<td><input type="checkbox" name="sastojci[]" value="';
					echo esc_attr($sirovina['sirovina_id']) . '" /></td>';
					echo '<td>' . $sirovina['sirovina_id'] . '</td>';
					echo '<td><a href="';
					echo add_query_arg( array(
					'page' => 'sapun-kalk',
					'id' => $sirovina['sirovina_id'] ),
					admin_url( 'options-general.php' ) );
					echo '">' . $sirovina['sirovina_naziv'] . '</a></td>';
					echo '<td>' . $sirovina['sap'] . '</td>';
					echo '<td>' . $sirovina['iodine'] . '</td>';
					echo '<td>' . $sirovina['ins'] . '</td>';
					echo '<td>' . $sirovina['lauric'] . '</td>';
					echo '<td>' . $sirovina['myristic'] . '</td>';
					echo '<td>' . $sirovina['palmitic'] . '</td>';
					echo '<td>' . $sirovina['stearic'] . '</td>';
					echo '<td>' . $sirovina['ricinoleic'] . '</td>';
					echo '<td>' . $sirovina['oleic'] . '</td>';
					echo '<td>' . $sirovina['linoleic'] . '</td>';
					echo '<td>' . $sirovina['linolenic'] .
					'</td></tr>';
				}
			} else {
					echo '<tr style="background: #FFF">';
					echo '<td colspan=14>Nema unesenih sirovina</td></tr>';
				}
			?>
			</table><br />
			<input type="submit" value="Delete Selected" class="button-primary"/>
</form>
	<?php } elseif ( isset( $_GET['id'] ) && ( $_GET['id'] == 'new' || is_numeric( $_GET['id'] ) ) ) {
					$sirovina_id = $_GET['id'];
					$sirovine_data = array();
					$mode = 'new';
						// Query database if numeric id is present
					if ( is_numeric( $sirovina_id ) ) {
						$sirovine_query = 'select * from ' . $wpdb->get_blog_prefix();
						$sirovine_query .= 'sapunkalk where sirovina_id = ' . $sirovina_id;
						$sirovine_data =
						$wpdb->get_row( $wpdb->prepare( $sirovine_query,10 ),
						ARRAY_A );
						// Set variable to indicate page mode
						if ( $sirovine_data ) $mode = 'edit';
					} else {
						$sirovine_data['sirovina_naziv'] = '';
						$sirovine_data['sap'] = '';
						$sirovine_data['iodine'] = '';
						$sirovine_data['ins'] = '';
						$sirovine_data['lauric'] = '';
						$sirovine_data['myristic'] = '';
						$sirovine_data['palmitic'] = '';
						$sirovine_data['stearic'] = '';
						$sirovine_data['ricinoleic'] = '';
						$sirovine_data['oleic'] = '';
						$sirovine_data['linoleic'] = '';
						$sirovine_data['linolenic'] = '';
						}
					// Display title based on current mode
					if ( $mode == 'new' ) {
						echo '<h3>Dodaj novu sirovinu</h3>';
					} elseif ( $mode == 'edit' ) {
						echo '<h3>Sirovina #' . $sirovine_data['sirovina_id'] . ' - ';
						echo $sirovine_data['sirovina_naziv'] . '</h3>';
					}
					?>
					<form method="post" action="<?php echo admin_url( 'admin-post.php' ); ?>"> 
						<input type="hidden" name="action" value="save_sapunkalk" />
						<input type="hidden" name="sirovina_id" value="<?php echo esc_attr( $sirovina_id ); ?>" />
						<!-- Adding security through hidden referrer field -->
						<?php wp_nonce_field( 'sapunkalk_add_edit' ); ?>
						<!-- Display bug editing form -->
					<table>
						<tr>
							<td style="width: 150px">Naziv</td>
							<td><input type="text" name="sirovina_naziv" size="60" value="<?php echo esc_attr($sirovine_data['sirovina_naziv'] ); ?>"/></td>
						</tr>
						<tr>
							<td>SAP</td>
							<td><input type="text" name="sap" value="<?php echo esc_attr($sirovine_data['sap'] ); ?>" /></td>
						</tr>
						<tr>
							<td>Iodine</td>
							<td><input type="text" name="iodine" value="<?php echo esc_attr($sirovine_data['iodine'] ); ?>" /></td>
						</tr>
						<tr>
							<td>INS</td>
							<td><input type="text" name="ins" value="<?php echo esc_attr($sirovine_data['ins'] ); ?>" /></td>
						</tr>
						<tr>
							<td>Lauric</td>
							<td><input type="text" name="lauric" value="<?php echo esc_attr($sirovine_data['lauric'] ); ?>" /></td>
						</tr>
						<tr>
							<td>Myristic</td>
							<td><input type="text" name="myristic" value="<?php echo esc_attr($sirovine_data['myristic'] ); ?>" /></td>
						</tr>
						<tr>
							<td>Stearic</td>
							<td><input type="text" name="stearic" value="<?php echo esc_attr($sirovine_data['stearic'] ); ?>" /></td>
						</tr>
						<tr>
							<td>Ricinoleic</td>
							<td><input type="text" name="ricinoleic" value="<?php echo esc_attr($sirovine_data['ricinoleic'] ); ?>" /></td>
						</tr>
						<tr>
							<td>Oleic</td>
							<td><input type="text" name="oleic" value="<?php echo esc_attr($sirovine_data['oleic'] ); ?>" /></td>
						</tr>
						<tr>
							<td>Linoleic</td>
							<td><input type="text" name="linoleic" value="<?php echo esc_attr($sirovine_data['linoleic'] ); ?>" /></td>
						</tr>
						<tr>
							<td>Linolenic</td>
							<td><input type="text" name="linolenic" value="<?php echo esc_attr($sirovine_data['linolenic'] ); ?>" /></td>
						</tr>
					</table>
					<input type="submit" value="Submit" class="button-primary"/>
				</form>
	<?php } ?>
	</div>
	<?php 
	
	
	
	
	
	}